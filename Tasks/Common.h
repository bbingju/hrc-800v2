#ifndef __COMMON_H_
#define __COMMON_H_

#include <stdint.h>
#include <stdbool.h>

#if defined(__ICCARM__)
#define DEBUG   1
//#define FEATURE_OTHER_ROOMS_CONTROL
#endif

#ifdef FEATURE_RTT_FOR_DEBUG
#define DBG_LOG(f_, ...)    SEGGER_RTT_printf(0, (f_), ##__VA_ARGS__)
#else
#define DBG_LOG(f_, ...)
#endif

#define SETTING_TEMP_DEFAULT    18.0f
#define ROOM_NBR_MAX            8
#define ROOM_TEMPERATURE_MIN    5.0f  // 설정가능한 최저 실내 온도
#define ROOM_TEMPERATURE_MAX    40.0f // 설정가능한 최고 실내 온도

#define COIL_LENGTH_MIN         0
#define COIL_LENGTH_MAX         40
#define COIL_LENGTH_DEFAULT     10

#define STATE_ROLLBACK_INTERVAL_MS          (30 * 1000)

enum _heat_mode {
    HEAT_MODE_OFF,
    HEAT_MODE_ON,
    HEAT_MODE_RUN,
};
typedef uint8_t heat_mode_t;

typedef struct _ROOM
{
    uint8_t id;

    heat_mode_t heat_mode;
    uint8_t coil_length;        /* 0 ~ 40 */
    bool reservation_mode_on;

    float CurrentTemperature;
    float SettingTemperature;   /* 설정 온도 */

    uint32_t reserved_time_block;
} ROOM;

#define ROOM_IS_HEATING(r)        ((r)->heat_mode == HEAT_MODE_ON || (r)->heat_mode == HEAT_MODE_RUN)
/* #define ROOM_HEAT_ON(r)           do {                                  \ */
/*         (r)->heat_mode =                                                \ */
/*             ((r)->CurrentTemperature < (r)->SettingTemperature) ?       \ */
/*             HEAT_MODE_RUN : HEAT_MODE_ON;                               \ */
/*     } while (0) */
#define ROOM_HEAT_ON(r)           do {          \
        (r)->heat_mode = HEAT_MODE_ON;          \
    } while (0)
#define ROOM_HEAT_TOGGLE(r)       do {          \
        if ((r)->heat_mode == HEAT_MODE_OFF) {  \
            ROOM_HEAT_ON(r);                    \
        } else {                                \
            (r)->heat_mode = HEAT_MODE_OFF;     \
        } } while (0)

#define CLEAR_MODIFIED_BITS(v)    ((v) = 0)
#define UNSET_MODIFIED_BIT(v, b)  ((v) = (v) & ~(1 << (b)))
#define SET_MODIFIED_BIT(v, b)    ((v) = (v) | (1 << (b)))
#define IS_MODIFIED_BIT_SET(v, b) ((v) & (1 << (b)))


typedef struct
{
    ROOM rooms[ROOM_NBR_MAX];
    ROOM *room;                      // 현재 LCD에 보여지는 방
    ROOM *coil_length_room;

    //float SettingRoomTemperatureBackup = 30.0f; // 설정 온도 백업용 (외출 모드 해제시 이전 상태 복원을 위해) */
    //float SettingWaterTemperature = 30.0f; // 온수 온도 설정값

    bool leave_mode_on;             // 외출모드
    bool set_time_just;

    uint8_t modified_bits;
    uint8_t modified_bits_heat_mode;

    struct {
        uint8_t hour;
        uint8_t min;
    } current_time;

    float temp_correction;
} CURRENT_STATE;

typedef enum
{
    GUI_STATE_NORMAL,               // 일반 상태
    GUI_STATE_LEAVE,                // 외출 상태
    GUI_STATE_RESERVATION_SET,      // 예약 설정 상태
    GUI_STATE_TIME_CONFIG,          // 시간 설정 상태
    GUI_STATE_CONFIG_PRE,           // 
    GUI_STATE_CONFIG_ROOMSELECTING, // 조절기 방지정 상태
    GUI_STATE_CONFIG_LOT,           // 전체방 개수 설정 상태
    GUI_STATE_CONFIG_TEMPCORRECTION,// 현재온도 보상 상태
    GUI_STATE_CONFIG_COILLENGTH,    // 코일길이 설정 상태
} GUI_STATES;

typedef enum
{
    LED_ON,                         // LED ON
    LED_OFF,                        // OFF
    LED_DIM                         // 최소 밝기 상태
} LED_STATES;

typedef struct
{
    uint16_t RoomNumber;
    uint16_t total_rooms;
    /* float CurrentRoomTemperatureOffset[4]; */
} DEVICE_CONFIG;

extern CURRENT_STATE CurrentState;
extern GUI_STATES CurrentGUI_State;
extern DEVICE_CONFIG DeviceConfig;

#ifdef __cplusplus
extern "C" {
#endif

    void LED_BacklightBrightUp();
    void LED_BrightnessUp();
    void ReserveSaveState();

#ifdef __cplusplus
}
#endif

#endif
