#include <stdio.h>
#include <string.h>

#include "stm32l0xx_hal.h"
#include "cmsis_os.h"
#ifdef FEATURE_RTT_FOR_DEBUG
#include "SEGGER_RTT.h"
#endif
#include "timers.h"

#include "Common.h"
#include "HRC-800V2_CustomLCD.h"
#include "nv.h"
#include "ramfunc.h"

//------------------------------------------------------------------------------
extern LCD_HandleTypeDef hlcd;
extern TIM_HandleTypeDef htim6;

//------------------------------------------------------------------------------
#ifdef __cplusplus
extern "C"
{
#endif

    void TaskGUI(void const *argument);
    void LED_Dimming();
    void LED_BrightnessUp();
    void do_save_if_pending();

#ifdef __cplusplus
}
#endif

__INLINE static void KeyScan();
__INLINE static void GUI_Control_HeatLED(CURRENT_STATE *);
__INLINE static void GUI_Control_RsrvLED(CURRENT_STATE *);

void vTimerCallbackBlink(TimerHandle_t xTimer);

void ReserveSaveState();
void ReserveSaveStateCoilLength();

//------------------------------------------------------------------------------
GUI_STATES CurrentGUI_State;
GUI_STATES PrevGUI_State;

enum {
    KEY_TYPE_HEAT = 0,
    KEY_TYPE_RSRV,
    KEY_TYPE_DOWN,
    KEY_TYPE_UP,
    KEY_TYPE_MAX
};

static bool KeyisLongPushed[KEY_TYPE_MAX];   // 버튼을 길게 누르고 있는 경우 set됨
static bool KeyisShortPushed[KEY_TYPE_MAX];  // 키가 짧게 눌렀다 떼는 경우 set됨
static bool KeyisPushing[KEY_TYPE_MAX];      // 키가 계속 눌려있는 경우 set됨
static uint32_t KeyPushedTick[KEY_TYPE_MAX]; // 키가 처음 눌린 tick (long push 감지용)

bool KeyisHandled = false; // 키 입력 처리가 완료되는 경우 true로 set

// TimerHandle_t xTimerBlinkControl;   // 소프트웨어 타이머 - LCD 세그먼트 깜빡임 제어용

static uint32_t LastBlinkTick = 0;
static bool     BlinkState = false;
static bool     PreCommited = false;
static uint8_t  HeatKeyShortPushCount = 0;
static uint8_t  RsrvKeyShortPushCount = 0;

uint32_t LastKeyPushedTick = 0; // Key LED 소등용

volatile uint8_t LED_BACKLIGHT_DUTY = 100;
volatile uint8_t LED_GREEN_DUTY = 0;
volatile uint8_t LED_RED_DUTY = 0;
volatile uint8_t LED_RESV_DUTY = 0;

bool SaveStatePending = false;
bool SaveStateCoilLengthPending = false;
// uint32_t LastSaveStateTick = 0;

static uint32_t StateEntryTick = 0;
static uint32_t value_update_tick;

static struct _time_config {
    enum { STATE_HOUR, STATE_MIN  } state;
    int hour;
    int min;
    bool blinked;
} time_config = {
    .state = STATE_HOUR,
    .hour = 0,
    .min = 0,
    .blinked = false,
};

static struct _temperal_config {
    uint16_t room_id;
    uint16_t total_rooms;
    ROOM *room;
    float temp_correction;
} temperal_config;

static void temperal_config_load(CURRENT_STATE *state, DEVICE_CONFIG *devinfo)
{
    temperal_config.room_id         = devinfo->RoomNumber;
    temperal_config.total_rooms     = devinfo->total_rooms;
    temperal_config.room            = &state->rooms[devinfo->RoomNumber - 1];
    temperal_config.temp_correction = state->temp_correction;
}

static void temperal_config_save(CURRENT_STATE *state, DEVICE_CONFIG *devinfo)
{
    devinfo->RoomNumber    = temperal_config.room_id;
    devinfo->total_rooms   = temperal_config.total_rooms;
    state->room            = &state->rooms[temperal_config.room_id - 1];
    state->temp_correction = temperal_config.temp_correction;
}

static GUI_STATES gui_states_transition_start(GUI_STATES new_state)
{
    // printf("[%s] %u ==> %u\r\n", __func__, CurrentGUI_State, new_state);
    PrevGUI_State = CurrentGUI_State;
    CurrentGUI_State = new_state;
    StateEntryTick = HAL_GetTick();
    return PrevGUI_State;
}

static GUI_STATES gui_states_transition_done()
{
    if (PrevGUI_State != CurrentGUI_State) {
        // printf("[%s] %u\r\n", __func__, CurrentGUI_State);
        PrevGUI_State = CurrentGUI_State;
    }
    return CurrentGUI_State;
}

static bool gui_states_is_changed()
{
    return PrevGUI_State != CurrentGUI_State;
}

__INLINE static void handle_state_normal()
{
    ROOM *current_room = CurrentState.room;

    if (HAL_GetTick() - StateEntryTick < 2 * 1000)
        return;

    if (!current_room)
        return;

    if (/* DeviceConfig.RoomNumber != 1 &&  */CurrentState.leave_mode_on)
    {
        KeyisLongPushed[KEY_TYPE_HEAT] = false;
        KeyisShortPushed[KEY_TYPE_HEAT] = false;
        KeyPushedTick[KEY_TYPE_HEAT] = HAL_GetTick();

        /* CurrentState.leave_mode_on = true; */
        /* ReserveSaveState(); */
        gui_states_transition_start(GUI_STATE_LEAVE);
        return;
    }

    if ((CurrentState.room->id != DeviceConfig.RoomNumber) &&
        (HAL_GetTick() - LastKeyPushedTick >= STATE_ROLLBACK_INTERVAL_MS))
    {
        CurrentState.room = &CurrentState.rooms[DeviceConfig.RoomNumber - 1];
        current_room = CurrentState.room;
        return;
    }

    // 설정 모드 진입 키 = 난방+예약(길게 누름) 후 난방 버튼 5회 입력
    if (!gui_states_is_changed() &&
        KeyisLongPushed[KEY_TYPE_HEAT] && KeyisLongPushed[KEY_TYPE_RSRV] &&
        !KeyisShortPushed[KEY_TYPE_HEAT] && !KeyisShortPushed[KEY_TYPE_RSRV])
    {
        HeatKeyShortPushCount = 0;
        RsrvKeyShortPushCount = 0;

        KeyisLongPushed[KEY_TYPE_HEAT] = false;
        KeyisLongPushed[KEY_TYPE_RSRV] = false;

        KeyPushedTick[KEY_TYPE_HEAT] = HAL_GetTick();
        KeyPushedTick[KEY_TYPE_RSRV] = HAL_GetTick();

        gui_states_transition_start(GUI_STATE_CONFIG_PRE);
        return;
    }

    static uint32_t heat_key_for_room_selecting_tick = 0;

    // 난방 버튼
    if ((HAL_GetTick() - heat_key_for_room_selecting_tick > 400) &&
        KeyisShortPushed[KEY_TYPE_HEAT] && !KeyisLongPushed[KEY_TYPE_HEAT] &&
        !KeyisShortPushed[KEY_TYPE_RSRV] && !KeyisLongPushed[KEY_TYPE_RSRV])
    {
        taskENTER_CRITICAL();
        ROOM_HEAT_TOGGLE(current_room);
        SET_MODIFIED_BIT(CurrentState.modified_bits_heat_mode, current_room->id - 1);
        taskEXIT_CRITICAL();

        if (CurrentState.room->id == DeviceConfig.RoomNumber)
            ReserveSaveState();
        return;
    }

#ifdef FEATURE_OTHER_ROOMS_CONTROL /* 방선택 기능 */
    if (DeviceConfig.RoomNumber == 1)
    {
        ROOM *next = CurrentState.room;
        int room_number = next->id;

        if (KeyisPushing[KEY_TYPE_HEAT] /* && (HAL_GetTick() - KeyPushedTick[KEY_TYPE_HEAT] > 300) */) {

            if (KeyisShortPushed[KEY_TYPE_UP]) {
                room_number = (room_number >= DeviceConfig.total_rooms) ? 1 : room_number + 1;
                next = &CurrentState.rooms[room_number - 1];
                if (next && next != current_room)
                    CurrentState.room = current_room = next;

                KeyisLongPushed[KEY_TYPE_HEAT] = false;
                KeyPushedTick[KEY_TYPE_HEAT] = HAL_GetTick();
                heat_key_for_room_selecting_tick = HAL_GetTick();
                return;
            }

            if (KeyisShortPushed[KEY_TYPE_DOWN]) {
                room_number = (room_number <= 1) ? DeviceConfig.total_rooms : room_number - 1;
                next = &CurrentState.rooms[room_number - 1];
                if (next && next != current_room)
                    CurrentState.room = current_room = next;

                KeyisLongPushed[KEY_TYPE_HEAT] = false;
                KeyPushedTick[KEY_TYPE_HEAT] = HAL_GetTick();
                heat_key_for_room_selecting_tick = HAL_GetTick();
                return;
            }
        }
    }
#endif  /* FEATURE_OTHER_ROOMS_CONTROL */

    // Setting Temperature
    if (ROOM_IS_HEATING(current_room) || current_room->reservation_mode_on)
    {
        if (!KeyisPushing[KEY_TYPE_HEAT])
        {
            float temp = current_room->SettingTemperature;

            /* Short Key - DOWN */
            if (KeyisShortPushed[KEY_TYPE_DOWN]) {
                temp = temp > ROOM_TEMPERATURE_MIN ? temp - 0.5f : ROOM_TEMPERATURE_MIN;
            }
            else if (KeyisShortPushed[KEY_TYPE_UP]) {
                temp = temp < ROOM_TEMPERATURE_MAX ? temp + 0.5f : ROOM_TEMPERATURE_MAX;
            }
            else {
                if (KeyisPushing[KEY_TYPE_DOWN] && HAL_GetTick() - KeyPushedTick[KEY_TYPE_DOWN] > 800) {
                    // 누르고 있는 경우 150ms 마다 반복하도록 함
                    if (HAL_GetTick() - value_update_tick > 120) {
                        temp = temp > ROOM_TEMPERATURE_MIN ? temp - 0.5f : ROOM_TEMPERATURE_MIN;
                        value_update_tick = HAL_GetTick();
                    }
                }

                if (KeyisPushing[KEY_TYPE_UP] && HAL_GetTick() - KeyPushedTick[KEY_TYPE_UP] > 800) {
                    // 누르고 있는 경우 150ms 마다 반복하도록 함
                    if (HAL_GetTick() - value_update_tick > 120) {
                        temp = temp < ROOM_TEMPERATURE_MAX ? temp + 0.5f : ROOM_TEMPERATURE_MAX;
                        value_update_tick = HAL_GetTick();
                    }
                }
            }

            if (current_room->SettingTemperature != temp) {
                taskENTER_CRITICAL();
                current_room->SettingTemperature = temp;
                SET_MODIFIED_BIT(CurrentState.modified_bits, current_room->id - 1);
                taskEXIT_CRITICAL();
                if (current_room->id == DeviceConfig.RoomNumber)
                    ReserveSaveState();
                return;
            }
        }
    }

    int interval = (KeyPushedTick[KEY_TYPE_RSRV] > KeyPushedTick[KEY_TYPE_HEAT]) ? \
        KeyPushedTick[KEY_TYPE_RSRV] - KeyPushedTick[KEY_TYPE_HEAT] : \
        KeyPushedTick[KEY_TYPE_HEAT] - KeyPushedTick[KEY_TYPE_RSRV];
    // 외출모드
    if (!CurrentState.leave_mode_on && DeviceConfig.RoomNumber == 1 && current_room->id == 1 && !gui_states_is_changed() && interval > 300 &&
        KeyisLongPushed[KEY_TYPE_HEAT] && !KeyisShortPushed[KEY_TYPE_HEAT] && !KeyisPushing[KEY_TYPE_RSRV] &&
        !KeyisShortPushed[KEY_TYPE_RSRV] && !KeyisShortPushed[KEY_TYPE_DOWN] && !KeyisShortPushed[KEY_TYPE_UP])
    {
        KeyisLongPushed[KEY_TYPE_HEAT] = false;
        KeyisShortPushed[KEY_TYPE_HEAT] = false;
        KeyPushedTick[KEY_TYPE_HEAT] = HAL_GetTick();

        taskENTER_CRITICAL();
        CurrentState.leave_mode_on = true;
        taskEXIT_CRITICAL();

        ReserveSaveState();
        gui_states_transition_start(GUI_STATE_LEAVE);
        return;
    }

    // Time configuration mode
    if (DeviceConfig.RoomNumber == 1)
    {
        ROOM *current_room = CurrentState.room;
        if (!gui_states_is_changed() &&
            (!ROOM_IS_HEATING(current_room) || !current_room->reservation_mode_on) &&
            KeyisLongPushed[KEY_TYPE_DOWN] && KeyisLongPushed[KEY_TYPE_UP])
        {
            KeyisLongPushed[KEY_TYPE_DOWN] = false;
            KeyisLongPushed[KEY_TYPE_UP] = false;
            KeyPushedTick[KEY_TYPE_DOWN] = HAL_GetTick();
            KeyPushedTick[KEY_TYPE_UP] = HAL_GetTick();

            time_config.state = STATE_HOUR;
            time_config.hour = CurrentState.current_time.hour;
            time_config.min = CurrentState.current_time.min;
            time_config.blinked = true;

            gui_states_transition_start(GUI_STATE_TIME_CONFIG);
            return;
        }

    }

    // 예약설정모드
    if (!gui_states_is_changed() &&
        (DeviceConfig.RoomNumber == CurrentState.room->id) &&
        KeyisLongPushed[KEY_TYPE_RSRV] &&
        !KeyisShortPushed[KEY_TYPE_HEAT] && !KeyisLongPushed[KEY_TYPE_HEAT] && !KeyisPushing[KEY_TYPE_HEAT])
    {
        HeatKeyShortPushCount = 0;
        KeyisLongPushed[KEY_TYPE_RSRV] = false;
        KeyPushedTick[KEY_TYPE_RSRV] = HAL_GetTick();

        time_config.state = STATE_HOUR;
        time_config.hour = 0;
        time_config.min = 0;
        time_config.blinked = true;

        gui_states_transition_start(GUI_STATE_RESERVATION_SET);
        return;
    }

    // 예약 버튼
    if (KeyisShortPushed[KEY_TYPE_RSRV] && !KeyisLongPushed[KEY_TYPE_RSRV] && !KeyisPushing[KEY_TYPE_HEAT])
    {
        current_room->reservation_mode_on = !current_room->reservation_mode_on;
        LED_RESV_DUTY = current_room->reservation_mode_on ? 100 : 0; // LED
        if (CurrentState.room->id != DeviceConfig.RoomNumber) {
            taskENTER_CRITICAL();
            SET_MODIFIED_BIT(CurrentState.modified_bits, current_room->id - 1);
            taskEXIT_CRITICAL();
        }
        else {
            ReserveSaveState();
        }
        return;
    }
}

__INLINE static void handle_state_reservation_set()
{
    if (HAL_GetTick() - StateEntryTick < 2 * 1000)
        return;

    if (HAL_GetTick() - LastKeyPushedTick >= STATE_ROLLBACK_INTERVAL_MS)
    {
        time_config.state = STATE_HOUR;
        time_config.blinked = false;
        gui_states_transition_start(GUI_STATE_NORMAL);
        return;
    }

    time_config.blinked = true;

    if (!gui_states_is_changed() && KeyisShortPushed[KEY_TYPE_RSRV] && !KeyisLongPushed[KEY_TYPE_RSRV]) // 예약 완료
    {
        HeatKeyShortPushCount = 0;
        KeyisShortPushed[KEY_TYPE_RSRV] = false;
        KeyisLongPushed[KEY_TYPE_RSRV] = false;
        KeyPushedTick[KEY_TYPE_RSRV] = HAL_GetTick();

        time_config.state = STATE_HOUR;
        time_config.blinked = false;
        ReserveSaveState();
        gui_states_transition_start(GUI_STATE_NORMAL);
        osDelay(500);
        return;
    }

    if (KeyisShortPushed[KEY_TYPE_HEAT] && CurrentState.room)    // set hour
    {
        uint32_t block = CurrentState.room->reserved_time_block;

        if (block & (1 << time_config.hour)) {
            CurrentState.room->reserved_time_block  = block & ~(1 << time_config.hour);
        }
        else {
            CurrentState.room->reserved_time_block  = block | (1 << time_config.hour);
        }

        time_config.hour = (time_config.hour == 23) ? 0 : (time_config.hour + 1);
    }

    // Down 버튼
    if (KeyisShortPushed[KEY_TYPE_DOWN] && !KeyisPushing[KEY_TYPE_HEAT])
    {
        time_config.hour = (time_config.hour == 0) ? 23 : (time_config.hour - 1);
        return;
    }

    // Up 버튼
    if (KeyisShortPushed[KEY_TYPE_UP] && !KeyisPushing[KEY_TYPE_HEAT])
    {
        time_config.hour = (time_config.hour == 23) ? 0 : (time_config.hour + 1);
        return;
    }
}

__INLINE static void handle_state_time_config()
{
    if (HAL_GetTick() - LastKeyPushedTick >= 10 * 1000)
    {
        time_config.state = STATE_HOUR;
        time_config.blinked = false;
        gui_states_transition_start(GUI_STATE_NORMAL);
        return;
    }

    if (time_config.state == STATE_HOUR)
    {
        time_config.blinked = true;

        if (KeyisShortPushed[KEY_TYPE_HEAT])
        {
            time_config.state = STATE_MIN;
        }

        if (KeyisShortPushed[KEY_TYPE_DOWN]) // Down Key
        {
            if (time_config.hour <= 0)
                time_config.hour = 23;
            else
                time_config.hour--;
        }
        else if (KeyisShortPushed[KEY_TYPE_UP]) // Up Key
        {
            if (time_config.hour >= 23)
                time_config.hour = 0;
            else
                time_config.hour++;
        }
        else {
            if (KeyisPushing[KEY_TYPE_DOWN] && HAL_GetTick() - KeyPushedTick[KEY_TYPE_DOWN] > 800) {
                // 누르고 있는 경우 150ms 마다 반복하도록 함
                if (HAL_GetTick() - value_update_tick > 120) {
                    if (time_config.hour <= 0)
                        time_config.hour = 23;
                    else
                        time_config.hour--;
                    value_update_tick = HAL_GetTick();
                }
            }
            else if (KeyisPushing[KEY_TYPE_UP] && HAL_GetTick() - KeyPushedTick[KEY_TYPE_UP] > 800) {
                // 누르고 있는 경우 150ms 마다 반복하도록 함
                if (HAL_GetTick() - value_update_tick > 120) {
                    if (time_config.hour >= 23)
                        time_config.hour = 0;
                    else
                        time_config.hour++;
                    value_update_tick = HAL_GetTick();
                }
            }
        }
    }
    else if (time_config.state == STATE_MIN)
    {
        if (KeyisShortPushed[KEY_TYPE_HEAT])
        {
            time_config.state = STATE_HOUR;
            time_config.blinked = false;
            vPortEnterCritical();
            CurrentState.current_time.hour = time_config.hour;
            CurrentState.current_time.min = time_config.min;
            CurrentState.set_time_just = 1;
            vPortExitCritical();
            gui_states_transition_start(GUI_STATE_NORMAL);
            vTaskDelay(500);
        }

        if (KeyisShortPushed[KEY_TYPE_DOWN]) // Down Key
        {
            if (time_config.min == 0)
                time_config.min = 59;
            else
                time_config.min--;
        }
        else if (KeyisShortPushed[KEY_TYPE_UP]) // Up Key
        {
            if (time_config.min == 59)
                time_config.min = 0;
            else
                time_config.min++;
        }
        else {
            if (KeyisPushing[KEY_TYPE_DOWN] && HAL_GetTick() - KeyPushedTick[KEY_TYPE_DOWN] > 800) {
                // 누르고 있는 경우 150ms 마다 반복하도록 함
                if (HAL_GetTick() - value_update_tick > 120) {
                    if (time_config.min <= 0)
                        time_config.min = 59;
                    else
                        time_config.min--;
                    value_update_tick = HAL_GetTick();
                }
            }

            if (KeyisPushing[KEY_TYPE_UP] && HAL_GetTick() - KeyPushedTick[KEY_TYPE_UP] > 800) {
                // 누르고 있는 경우 150ms 마다 반복하도록 함
                if (HAL_GetTick() - value_update_tick > 120) {
                    if (time_config.min >= 59)
                        time_config.min = 0;
                    else
                        time_config.min++;
                    value_update_tick = HAL_GetTick();
                }
            }
        }
    }
}

static const char *temperature_string(const float temp)
{
    static char temp_str[5] = {0};

    if (temp > 199.9f || temp < -9.9f)
        return temp_str;

    if (temp > 0.0f) {
        if (temp <= 199.9f && temp >= 100.0f) {
            int temp_int = (int)temp - 100;
            temp_str[0] = '1';
            temp_str[1] = '0' + (temp_int / 10);
            temp_str[2] = '0' + (temp_int % 10);
            temp_str[3] = '0' + (((int)((temp - 100.0f) * 10.0f)) % 10);
        } else if (temp < 100.0f && temp >= 10.0f) {
            temp_str[0] = ' ';
            temp_str[1] = '0' + (((int)(temp)) / 10);
            temp_str[2] = '0' + (((int)(temp)) % 10);
            temp_str[3] = '0' + (((int)(temp * 10.0f)) % 10);
        } else {
            temp_str[0] = ' ';
            temp_str[1] = ' ';
            temp_str[2] = '0' + ((int)temp);
            temp_str[3] = '0' + (((int)(temp * 10.0f)) % 10);
        }
    }
    else {
        if (temp == 0.0f) {
            temp_str[0] = ' ';
            temp_str[1] = ' ';
            temp_str[2] = '0';
            temp_str[3] = '0';
        } else {
            temp_str[0] = ' ';
            temp_str[1] = '-';
            temp_str[2] = '0' + ((int)(temp * -1));
            temp_str[3] = '0' + (((int)(temp * -10.0f)) % 10);
        }
    }

    temp_str[4] = 0;

    return temp_str;
}

static void show_time_digit()
{
    static bool _enabled = true;

    LCD__SETTING_TEMP(false);
    LCD__SETTING_TEMP_POINT(false);
    LCD__TEMP_C(false);
    LCD__TIME_SEPERATOR(true);

    char time[4] = {0};

    if (!time_config.blinked)
    {
        time[0] = '0' + (CurrentState.current_time.hour / 10);
        time[1] = '0' + (CurrentState.current_time.hour % 10);
        time[2] = '0' + (CurrentState.current_time.min / 10);
        time[3] = '0' + (CurrentState.current_time.min % 10);

        _enabled = true;
        draw_4digit_right(time);
    }
    else
    {
        time[0] = '0' + (time_config.hour / 10);
        time[1] = '0' + (time_config.hour % 10);
        time[2] = '0' + (time_config.min / 10);
        time[3] = '0' + (time_config.min % 10);

        if (time_config.state == STATE_HOUR)
        {
            if (_enabled)
                time[0] = time[1] = ' ';
            draw_4digit_right(time);
        }
        else
        {
            if (_enabled)
                time[2] = time[3] = ' ';
            draw_4digit_right(time);
        }

        if (HAL_GetTick() - LastBlinkTick > 250) {
            _enabled = !_enabled;
            LastBlinkTick = HAL_GetTick();
        }
    }
}

__INLINE static void DrawLCD()
{
    // ------------------------------------------------------------------------------
    // 화면 업데이트 - 공통
    // ------------------------------------------------------------------------------
    ROOM *current_room = CurrentState.room;
    if (!current_room)
        return;

    GUI_Control_HeatLED(&CurrentState);
    GUI_Control_RsrvLED(&CurrentState);

    if (HAL_GetTick() - LastKeyPushedTick >= 10 * 1000)
    {
        // 버튼을 누른지 10초가 경과하면 Up/Down 버튼의 LED 소등함
        HAL_GPIO_WritePin(GPIOC, GPIO_PIN_12, GPIO_PIN_RESET);

        // 다른 LED 밝기도 낮춤
        if (LED_RESV_DUTY > 5)
            LED_RESV_DUTY = 5;

        if (LED_RED_DUTY > 5)
            LED_RED_DUTY = 5;

        if (LED_GREEN_DUTY > 5)
            LED_GREEN_DUTY = 5;

	if (!(SaveStatePending || SaveStateCoilLengthPending))
            LED_BACKLIGHT_DUTY = 5;
    }

    // 방 번호 표시 (1~8)
    if (CurrentGUI_State == GUI_STATE_CONFIG_ROOMSELECTING) {
        if (HAL_GetTick() - LastBlinkTick > 250)
        {
            LCD_WriteChar(LCD_RoomNumber_MAP[0], PreCommited ? temperal_config.room_id : (BlinkState ? ' ' : temperal_config.room_id));
            BlinkState = !BlinkState;
            LastBlinkTick = HAL_GetTick();
        }
    }
    else if (CurrentGUI_State == GUI_STATE_CONFIG_LOT) {
        if (HAL_GetTick() - LastBlinkTick > 250)
        {
            LCD_WriteChar(LCD_RoomNumber_MAP[0], BlinkState ? ' ' : temperal_config.total_rooms);
            BlinkState = !BlinkState;
            LastBlinkTick = HAL_GetTick();
        }
    }
    else {
        LCD_WriteChar(LCD_RoomNumber_MAP[0], CurrentState.room->id);
    }

    // Left Digits
    if (CurrentGUI_State == GUI_STATE_CONFIG_COILLENGTH) {
        LCD__CURRENT_TEMP(false);
        LCD__CURRENT_TEMP_POINT(false);
        LCD__TEMP_C(false);

        char coil_length_str[8] = { 0 };
        sprintf(&coil_length_str[1], "%2u", (unsigned int) (CurrentState.coil_length_room->coil_length));
        coil_length_str[0] = ' ';
        coil_length_str[3] = ' ';
        draw_4digit_left(coil_length_str);
    }
    else if (CurrentGUI_State == GUI_STATE_CONFIG_TEMPCORRECTION) {
        LCD__CURRENT_TEMP(false);
        LCD__CURRENT_TEMP_POINT(true);
        LCD__TEMP_C(false);

        draw_4digit_left(temperature_string(temperal_config.temp_correction));
    }
    else {                      // 현재 온도 표시
        LCD__CURRENT_TEMP(true);
        LCD__CURRENT_TEMP_POINT(true);
        LCD__TEMP_C(true);

        char *temp_str = (char *) temperature_string(CurrentState.room->CurrentTemperature);
        temp_str[3] = (temp_str[3] < '5') ? '0' : '5'; // 현제 온도는 0.5도 단위로 표시
        draw_4digit_left(temp_str);
    }

    // Right Digits
    if (CurrentGUI_State == GUI_STATE_NORMAL) {
        if (CurrentState.room->id == DeviceConfig.RoomNumber) {
            if (!ROOM_IS_HEATING(CurrentState.room) && !CurrentState.room->reservation_mode_on)
                show_time_digit();
            else {
                LCD__SETTING_TEMP(true);
                LCD__SETTING_TEMP_POINT(true);
                LCD__TEMP_C(true);
                LCD__TIME_SEPERATOR(false);

                char temp_str[5] = { 0 };
                sprintf(temp_str, "%3u", (int)(current_room->SettingTemperature * 10.0f));
                if (temp_str[1] == ' ')
                    temp_str[1] = '0';
                if (temp_str[2] == ' ')
                    temp_str[2] = '0';
                temp_str[3] = ' ';
                draw_4digit_right(temp_str);
            }
        }
        else {
            LCD__SETTING_TEMP(true);
            LCD__SETTING_TEMP_POINT(true);
            LCD__TEMP_C(true);
            LCD__TIME_SEPERATOR(false);

            char temp_str[5] = { 0 };
            sprintf(temp_str, "%3u", (int)(current_room->SettingTemperature * 10.0f));
            if (temp_str[1] == ' ')
                temp_str[1] = '0';
            if (temp_str[2] == ' ')
                temp_str[2] = '0';
            temp_str[3] = ' ';
            draw_4digit_right(temp_str);
        }
    }
    else if (CurrentGUI_State == GUI_STATE_TIME_CONFIG) {
        show_time_digit();
    }
    else if (CurrentGUI_State == GUI_STATE_RESERVATION_SET) {
            LCD__SETTING_TEMP(true);
            LCD__SETTING_TEMP_POINT(true);
            LCD__TEMP_C(true);
            LCD__TIME_SEPERATOR(false);

            char temp_str[5] = { 0 };
            sprintf(temp_str, "%3u", (int)(current_room->SettingTemperature * 10.0f));
            if (temp_str[1] == ' ')
                temp_str[1] = '0';
            if (temp_str[2] == ' ')
                temp_str[2] = '0';
            temp_str[3] = ' ';
            draw_4digit_right(temp_str);
    }
    else
    {
        if (CurrentGUI_State == GUI_STATE_LEAVE // && CurrentState.leave_mode_on
           ) // 외출 시 'Out' 표시
        {
            LCD__SETTING_TEMP(false);
            LCD__SETTING_TEMP_POINT(false);
            LCD__TIME_SEPERATOR(false);
            LCD__TEMP_C(false);

            draw_4digit_right(" OFF");
        }
        else if (CurrentGUI_State == GUI_STATE_CONFIG_ROOMSELECTING)
        {
            LCD__SETTING_TEMP(false);
            LCD__SETTING_TEMP_POINT(false);
            LCD__TIME_SEPERATOR(false);
            LCD__TEMP_C(false);

            draw_4digit_right("  Id");
        }
        else if (CurrentGUI_State == GUI_STATE_CONFIG_LOT)
        {
            LCD__SETTING_TEMP(false);
            LCD__SETTING_TEMP_POINT(false);
            LCD__TIME_SEPERATOR(false);
            LCD__TEMP_C(false);

            draw_4digit_right(" Lot");
        }
        else if (CurrentGUI_State == GUI_STATE_CONFIG_TEMPCORRECTION)
        {
            LCD__SETTING_TEMP(false);
            LCD__SETTING_TEMP_POINT(false);
            LCD__TIME_SEPERATOR(false);
            LCD__TEMP_C(false);

            draw_4digit_right(" Adj");
        }
        else if (CurrentGUI_State == GUI_STATE_CONFIG_COILLENGTH)
        {
            LCD__SETTING_TEMP(false);
            LCD__SETTING_TEMP_POINT(false);
            LCD__TIME_SEPERATOR(true);
            LCD__TEMP_C(false);
            char coxl[5] = { 0 };
            sprintf(coxl, "Cor%c", CurrentState.coil_length_room->id);
            draw_4digit_right(coxl);
        }
    }


    // 예약 설정 표시
    if (CurrentGUI_State == GUI_STATE_RESERVATION_SET)
    {
        static bool _enabled = true;

        LCD_SetSegmentEx(4, 6, false);
        LCD_Update_X_Segment(6, _enabled);
        LCD_Update_B_Segments(current_room->reserved_time_block, time_config.hour, true);
        LCD_Update_B_Segment_By_Index(time_config.hour, _enabled);

        /* LED_RESV_DUTY = _enabled ? 100 : 0; */
        // HAL_GPIO_WritePin(GPIOB, GPIO_PIN_2, 1 ? GPIO_PIN_SET : GPIO_PIN_RESET);
        // HAL_Delay(3);

        if (HAL_GetTick() - LastBlinkTick > 400) {
            _enabled = !_enabled;
            LastBlinkTick = HAL_GetTick();
        }
    }
    else
    {
        if (CurrentGUI_State == GUI_STATE_NORMAL && current_room->reservation_mode_on)
        {
            LCD_SetSegmentEx(4, 6, false);
            LCD_Update_X_Segment(6, true);
            LCD_Update_B_Segments(current_room->reserved_time_block, -1, true);
            // LED_RESV_DUTY = 100;
        }
        else
        {
            /* LCD_ClearSegment(4, 6); */
	    LCD_SetSegmentEx(4, 6, true);
            LCD_Update_X_Segment(6, false);
            LCD_Update_B_Segments(0, -1, false);
            /* LED_RESV_DUTY = 0; */
        }
    }

    // 업데이트 사항을 LCD에 반영
    HAL_LCD_UpdateDisplayRequest(&hlcd);
}

//------------------------------------------------------------------------------
/* TaskGUI function */
void TaskGUI(void const *argument)
{
    ROOM *current_room  = CurrentState.room;

    // 소프트웨어 타이머 생성
    // xTimerBlinkControl = xTimerCreate("TimerBlink",
    //                                   500,    // 500ms 주기
    //                                   pdTRUE, // 반복 ON
    //                                   (void *)0,
    //                                   vTimerCallbackBlink // 콜백 함수
    // );

    // xTimerStart(xTimerBlinkControl, 10);

    HAL_LCD_Clear(&hlcd);

    // 기본적으로 켜지는 세그먼트
    /* if (current_room && current_room->reservation_mode_on) */
    /* { */
    /*     LED_RESV_DUTY = 100; */
    /* } */

    //LCD_Update_B_Segments(24);

    LCD_Update_X_Segment(7, true);  // "Room\nNo."
    LCD_Update_X_Segment(9, true);  // Horizental Line
    LCD__CURRENT_TEMP(true);
    LCD__CURRENT_TEMP_POINT(true);
    LCD__SETTING_TEMP(false);
    LCD__SETTING_TEMP_POINT(false);
    LCD__TEMP_C(false);

    HAL_LCD_UpdateDisplayRequest(&hlcd);

    HAL_GPIO_WritePin(GPIOC, GPIO_PIN_12, GPIO_PIN_RESET);

    if (CurrentState.leave_mode_on) {
        gui_states_transition_start(GUI_STATE_LEAVE);
    } else {
        gui_states_transition_start(GUI_STATE_NORMAL);
    }

    /* Infinite loop */
    for (;;)
    {
        // ------------------------------------------------------------------------------
        // 키 입력 처리
        // ------------------------------------------------------------------------------
        // Key 스캔
        KeyScan();

        for (int i = 0; i < 4; i++)
        {
            if (KeyisLongPushed[i] == false && KeyisShortPushed[i] == false && KeyisPushing[i] == false) {
                gui_states_transition_done();
            }
        }

        // 키 입력에 따른 동작
        // .....
        KeyisHandled = false;

        switch (CurrentGUI_State)
        {
        // --------------------------------------------------------------------
        // 기본 상태
        // --------------------------------------------------------------------
        case GUI_STATE_NORMAL:
        {
            handle_state_normal();
            break;
        }

        // --------------------------------------------------------------------
        // 예약 설정 상태
        // --------------------------------------------------------------------
        case GUI_STATE_RESERVATION_SET:
        {
            handle_state_reservation_set();
            break;
        }

        case GUI_STATE_LEAVE:
        {
            if (HAL_GetTick() - StateEntryTick < 2000)
                break;

            if (!CurrentState.leave_mode_on)
            {
                gui_states_transition_start(GUI_STATE_NORMAL);
                break;
            }

            if (DeviceConfig.RoomNumber == 1 && KeyisLongPushed[KEY_TYPE_HEAT] && !KeyisShortPushed[KEY_TYPE_HEAT])
            {
                KeyisShortPushed[KEY_TYPE_HEAT] = false;
                KeyisLongPushed[KEY_TYPE_HEAT] = false;
                KeyPushedTick[KEY_TYPE_HEAT] = HAL_GetTick();

                taskENTER_CRITICAL();
                CurrentState.leave_mode_on = false;
                gui_states_transition_start(GUI_STATE_NORMAL);
                taskEXIT_CRITICAL();
                ReserveSaveState();
                break;
            }
            break;
        }

        case GUI_STATE_TIME_CONFIG:
        {
            handle_state_time_config();
            break;
        }

        // --------------------------------------------------------------------
        // 기능 설정 상태
        // --------------------------------------------------------------------
        case GUI_STATE_CONFIG_PRE:
        {
            if (HAL_GetTick() - StateEntryTick < 600)
                break;

            if (HAL_GetTick() - LastKeyPushedTick >= 10 * 1000)
            {
                KeyPushedTick[KEY_TYPE_HEAT] = HAL_GetTick();
                KeyPushedTick[KEY_TYPE_RSRV] = HAL_GetTick();
                HeatKeyShortPushCount = 0;
                gui_states_transition_start(GUI_STATE_NORMAL);
                break;
            }

            if (KeyisShortPushed[KEY_TYPE_HEAT])
            {
                if (++HeatKeyShortPushCount >= 5)
                {
                    HeatKeyShortPushCount = 0;
                    PreCommited = false;
                    temperal_config_load(&CurrentState, &DeviceConfig);
                    gui_states_transition_start(GUI_STATE_CONFIG_ROOMSELECTING);
                }
            }
            else if (KeyisShortPushed[KEY_TYPE_RSRV])
            {
                if (++RsrvKeyShortPushCount >= 5)
                {
                    RsrvKeyShortPushCount = 0;
                    if (DeviceConfig.RoomNumber == 1) {
                        CurrentState.coil_length_room = &CurrentState.rooms[0];
                        gui_states_transition_start(GUI_STATE_CONFIG_COILLENGTH);
                    }
                }
            }
            break;
        }

        case GUI_STATE_CONFIG_ROOMSELECTING:  // 방 지정 상태
        {
            // 입력이 없으면 Normal state로 되돌림
            if (HAL_GetTick() - LastKeyPushedTick >= 10 * 1000)
            {
                // nv_load_room_related(&CurrentState, &DeviceConfig);
                // CurrentState.room = &CurrentState.rooms[DeviceConfig.RoomNumber - 1];

                KeyPushedTick[KEY_TYPE_HEAT] = HAL_GetTick();
                KeyPushedTick[KEY_TYPE_RSRV] = HAL_GetTick();
                PreCommited = false;
                gui_states_transition_start(GUI_STATE_NORMAL);
                break;
            }

            if (KeyisShortPushed[KEY_TYPE_RSRV] && !KeyisLongPushed[KEY_TYPE_RSRV]) {
                if (DeviceConfig.RoomNumber == 1) {
                    if (PreCommited) {
                        // temperal_config.total_rooms = CurrentState.total_rooms;
                        gui_states_transition_start(GUI_STATE_CONFIG_LOT);
                    }
                }
                else {
                    if (PreCommited) {
                        // temp_correction = CurrentState.temp_correction;
                        gui_states_transition_start(GUI_STATE_CONFIG_TEMPCORRECTION);
                    }
                }
                break;
            }

            if (KeyisShortPushed[KEY_TYPE_HEAT])
            {
                if (PreCommited) {
                    // HeatKeyShortPushCount = 0;
                    // DeviceConfig.RoomNumber = temperal_config.room_id;
                    // CurrentState.room = &CurrentState.rooms[DeviceConfig.RoomNumber - 1];
                    gui_states_transition_start(GUI_STATE_NORMAL);
                }
                else {
                    // DeviceConfig.RoomNumber = room_selecting_temp;
                    // CurrentState.room = &CurrentState.rooms[DeviceConfig.RoomNumber - 1];
                    temperal_config_save(&CurrentState, &DeviceConfig);
                    ReserveSaveState();
                    PreCommited = true;
                }
                break;
            }
            else
            {
                if (KeyisShortPushed[KEY_TYPE_DOWN]) { // down key
                    PreCommited = false;
                    temperal_config.room_id = (temperal_config.room_id <= 1) ? DeviceConfig.total_rooms : temperal_config.room_id - 1;
                }
                else if (KeyisShortPushed[KEY_TYPE_UP]) { // up key
                    PreCommited = false;
                    temperal_config.room_id = (temperal_config.room_id >= DeviceConfig.total_rooms) ? 1 : temperal_config.room_id + 1;
                }
            }
            break;
        }

        case GUI_STATE_CONFIG_LOT:
        {
            if (HAL_GetTick() - LastKeyPushedTick >= 10 * 1000)
            {
                // nv_load_room_related(&CurrentState, &DeviceConfig);
                // CurrentState.room = &CurrentState.rooms[DeviceConfig.RoomNumber - 1];

                KeyPushedTick[KEY_TYPE_HEAT] = HAL_GetTick();
                KeyPushedTick[KEY_TYPE_RSRV] = HAL_GetTick();
                HeatKeyShortPushCount = 0;
                gui_states_transition_start(GUI_STATE_NORMAL);
                break;
            }

            if (KeyisShortPushed[KEY_TYPE_RSRV] && !KeyisLongPushed[KEY_TYPE_RSRV]) // Transition to Current Temp. Correction
            {
                // temp_correction = CurrentState.temp_correction;
                gui_states_transition_start(GUI_STATE_CONFIG_TEMPCORRECTION);
                break;
            }

            if (KeyisShortPushed[KEY_TYPE_HEAT] && !KeyisLongPushed[KEY_TYPE_HEAT]) // Save
            {
                // DeviceConfig.total_rooms = temperal_config.total_rooms;
                temperal_config_save(&CurrentState, &DeviceConfig);
                ReserveSaveState();

                gui_states_transition_start(GUI_STATE_NORMAL);
                break;
            }
            else
            {
                if (KeyisShortPushed[KEY_TYPE_DOWN])
                {
                    temperal_config.total_rooms = (temperal_config.total_rooms <= 1) ? ROOM_NBR_MAX : temperal_config.total_rooms - 1;
                }
                else if (KeyisShortPushed[KEY_TYPE_UP])
                {
                    temperal_config.total_rooms = (temperal_config.total_rooms >= ROOM_NBR_MAX) ? 1 : temperal_config.total_rooms + 1;
                }
            }
            break;
        }

        case GUI_STATE_CONFIG_TEMPCORRECTION:
        {
            if (HAL_GetTick() - LastKeyPushedTick >= 10 * 1000)
            {
                // nv_load_room_related(&CurrentState, &DeviceConfig);
                // CurrentState.room = &CurrentState.rooms[DeviceConfig.RoomNumber - 1];

                KeyPushedTick[KEY_TYPE_HEAT] = HAL_GetTick();
                KeyPushedTick[KEY_TYPE_RSRV] = HAL_GetTick();
                gui_states_transition_start(GUI_STATE_NORMAL);
                break;
            }

            if (KeyisShortPushed[KEY_TYPE_RSRV] && !KeyisLongPushed[KEY_TYPE_RSRV]) // Transition to Room Selecting
            {
                PreCommited = false;
                gui_states_transition_start(GUI_STATE_CONFIG_ROOMSELECTING);
                break;
            }

            if (KeyisShortPushed[KEY_TYPE_HEAT]) { // Save
                temperal_config_save(&CurrentState, &DeviceConfig);
                // CurrentState.temp_correction = temperal_config.temp_correction;
                ReserveSaveState();
                gui_states_transition_start(GUI_STATE_NORMAL);
                break;
            }
            else
            {
                if (KeyisShortPushed[KEY_TYPE_DOWN]) { // key down
                    temperal_config.temp_correction = (temperal_config.temp_correction <= -5.0f) ? -5.0f : temperal_config.temp_correction - 0.1f;
                }
                else if (KeyisShortPushed[KEY_TYPE_UP]) { // key up
                    temperal_config.temp_correction = (temperal_config.temp_correction >= 5.0f) ? 5.0f : temperal_config.temp_correction + 0.1f;
                }
                else {
                    if (KeyisPushing[KEY_TYPE_DOWN] && HAL_GetTick() - KeyPushedTick[KEY_TYPE_DOWN] > 800) {
                        // 누르고 있는 경우 150ms 마다 반복하도록 함
                        if (HAL_GetTick() - value_update_tick > 120) {
                            temperal_config.temp_correction = (temperal_config.temp_correction <= -5.0f) ? -5.0f : temperal_config.temp_correction - 0.1f;
                            value_update_tick = HAL_GetTick();
                        }
                    }
                    else if (KeyisPushing[KEY_TYPE_UP] && HAL_GetTick() - KeyPushedTick[KEY_TYPE_UP] > 800) {
                        // 누르고 있는 경우 150ms 마다 반복하도록 함
                        if (HAL_GetTick() - value_update_tick > 120) {
                            temperal_config.temp_correction = (temperal_config.temp_correction >= 5.0f) ? 5.0f : temperal_config.temp_correction + 0.1f;
                            value_update_tick = HAL_GetTick();
                        }
                    }
                }
            }
            break;
        }

        case GUI_STATE_CONFIG_COILLENGTH:
        {
            if (HAL_GetTick() - LastKeyPushedTick >= 10 * 1000)
            {
                KeyPushedTick[KEY_TYPE_HEAT] = HAL_GetTick();
                KeyPushedTick[KEY_TYPE_RSRV] = HAL_GetTick();
                RsrvKeyShortPushCount = 0;
                gui_states_transition_start(GUI_STATE_NORMAL);
                break;
            }

            if (CurrentState.coil_length_room) {
                ROOM *r = CurrentState.coil_length_room;

                if (KeyisShortPushed[KEY_TYPE_HEAT]) {
                    if (r->id < 8) {
                        CurrentState.coil_length_room = &CurrentState.rooms[r->id];
                    }
                    else {
                        ReserveSaveStateCoilLength();
                        CurrentState.coil_length_room = &CurrentState.rooms[0];
                        gui_states_transition_start(GUI_STATE_NORMAL);
                    }
                }
                else if (KeyisShortPushed[KEY_TYPE_DOWN]) {
                    r->coil_length--;
                    if (r->coil_length <= COIL_LENGTH_MIN)
                        r->coil_length = COIL_LENGTH_MIN;
                }
                else if (KeyisShortPushed[KEY_TYPE_UP]) {
                    r->coil_length++;
                    if (r->coil_length > COIL_LENGTH_MAX)
                        r->coil_length = COIL_LENGTH_MAX;
                }
                else {
                    if (KeyisPushing[KEY_TYPE_DOWN] && HAL_GetTick() - KeyPushedTick[KEY_TYPE_DOWN] > 800) {
                        // 누르고 있는 경우 150ms 마다 반복하도록 함
                        if (HAL_GetTick() - value_update_tick > 120) {
                            r->coil_length = (r->coil_length > COIL_LENGTH_MIN) ? r->coil_length - 1 : COIL_LENGTH_MIN;
                            value_update_tick = HAL_GetTick();
                        }
                    }
                    else if (KeyisPushing[KEY_TYPE_UP] && HAL_GetTick() - KeyPushedTick[KEY_TYPE_UP] > 800) {
                        // 누르고 있는 경우 150ms 마다 반복하도록 함
                        if (HAL_GetTick() - value_update_tick > 120) {
                            r->coil_length = (r->coil_length < COIL_LENGTH_MAX) ? r->coil_length + 1 : COIL_LENGTH_MAX;
                            value_update_tick = HAL_GetTick();
                        }
                    }
                }
            }
            break;
        }

        default:
            break;
        }

        KeyisHandled = true; // 키 입력 처리 완료

        DrawLCD();
        do_save_if_pending();
    }
    /* USER CODE END 5 */
}

//------------------------------------------------------------------------------
__INLINE static void KeyScan()
{
    static bool KeyState[4];     // 현재 키 눌림 상태
    static bool PrevKeyState[4]; // 이전 키 눌림 상태

    memcpy(PrevKeyState, KeyState, sizeof(bool) * 4);

    if (HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_9) == GPIO_PIN_RESET)
    {
        KeyState[KEY_TYPE_HEAT] = true;
        LastKeyPushedTick = HAL_GetTick();
        HAL_GPIO_WritePin(GPIOC, GPIO_PIN_12, GPIO_PIN_SET);
        LED_BrightnessUp();
    }
    else
        KeyState[KEY_TYPE_HEAT] = false;

    if (HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_13) == GPIO_PIN_RESET)
    {
        KeyState[1] = true;
        LastKeyPushedTick = HAL_GetTick();
        HAL_GPIO_WritePin(GPIOC, GPIO_PIN_12, GPIO_PIN_SET);
        LED_BrightnessUp();
    }
    else
        KeyState[1] = false;

    if (HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_14) == GPIO_PIN_RESET)
    {
        KeyState[2] = true;
        LastKeyPushedTick = HAL_GetTick();
        HAL_GPIO_WritePin(GPIOC, GPIO_PIN_12, GPIO_PIN_SET);
        LED_BrightnessUp();
    }
    else
        KeyState[2] = false;

    if (HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_15) == GPIO_PIN_RESET)
    {
        KeyState[3] = true;
        LastKeyPushedTick = HAL_GetTick();
        HAL_GPIO_WritePin(GPIOC, GPIO_PIN_12, GPIO_PIN_SET);
        LED_BrightnessUp();
    }
    else
        KeyState[3] = false;

    for (int i = 0; i < 4; i++)
    {
        if (PrevKeyState[i] == false && KeyState[i] == true)
        {
            if (KeyisPushing[i] == false)
                // 키가 처음 눌린 경우
                KeyPushedTick[i] = HAL_GetTick();
            else                // debouncing case
                KeyState[i] = false;
        }

        if (PrevKeyState[i] == true && KeyState[i] == false)
        {
            // 키가 눌렸다 떼어진 경우 (500ms 이내)
            if ((HAL_GetTick() - KeyPushedTick[i]) < 500)
            {
                // 짧게 눌렀다 떼는 경우
                KeyisShortPushed[i] = true;
            }
        }

        if (PrevKeyState[i] == true && KeyState[i] == true)
        {
            // 키를 누르고 있는 경우
            KeyisPushing[i] = true;

            // 3초 이상 누른경우 LongPush로 인식
            if ((HAL_GetTick() - KeyPushedTick[i]) >= 3000)
            {
                KeyisLongPushed[i] = true;
            }
        }

        if (PrevKeyState[i] == false && KeyState[i] == false)
        {
            // 키가 떼어진 상태인 경우
            if (KeyisHandled)
            {
                KeyisLongPushed[i] = false;
                KeyisShortPushed[i] = false;
                KeyisPushing[i] = false;
            }
        }
    }
}

//------------------------------------------------------------------------------
__INLINE static void GUI_Control_HeatLED(CURRENT_STATE *s)
{
    if (s && s->room)
    {
        if (s->leave_mode_on) {
            LCD_Update_X_Segment(1, false);
            LED_RED_DUTY = 0;
            /* LCD_Update_X_Segment(1, (s->room->heat_mode == HEAT_MODE_RUN) ? true : false); */
            /* LED_RED_DUTY = (s->room->heat_mode == HEAT_MODE_RUN) ? 100 : 0; */
            LED_GREEN_DUTY = 0;
        }
        else {
            switch (s->room->heat_mode) {
            case HEAT_MODE_OFF:
                LCD_Update_X_Segment(1, false);
                LED_RED_DUTY = 0;
                LED_GREEN_DUTY = 0;
                break;
            case HEAT_MODE_ON:
                LCD_Update_X_Segment(1, false);
                LED_RED_DUTY = 0;
                LED_GREEN_DUTY = 100;
                break;
            case HEAT_MODE_RUN:
                LCD_Update_X_Segment(1, true);
                LED_RED_DUTY = 100;
                LED_GREEN_DUTY = 0;
                break;
            default:
                break;
            }
        }
    }
}

__INLINE static void GUI_Control_RsrvLED(CURRENT_STATE *s)
{
    if (s && s->room)
    {
        if (s->leave_mode_on) {
            LED_RESV_DUTY = 0;
        }
        else {
            LED_RESV_DUTY = s->room->reservation_mode_on ? 100 : 0;
        }
    }
}

//------------------------------------------------------------------------------
void vTimerCallbackBlink(TimerHandle_t xTimer)
{
}


//------------------------------------------------------------------------------
void ReserveSaveState()
{
    SaveStatePending = true;
    // LastSaveStateTick = HAL_GetTick();
}

void ReserveSaveStateCoilLength()
{
    SaveStateCoilLengthPending = true;
}

//------------------------------------------------------------------------------
uint8_t led_duty_counter = 0;

void LED_BacklightBrightUp()
{
    LED_BACKLIGHT_DUTY = 100;
    led_duty_counter = 0;
}

void LED_BrightnessUp()
{
    if (LED_RESV_DUTY > 0)
        LED_RESV_DUTY = 100;
    if (LED_RED_DUTY > 0)
        LED_RED_DUTY = 100;
    if (LED_GREEN_DUTY > 0)
        LED_GREEN_DUTY = 100;

    LED_BacklightBrightUp();
}

//------------------------------------------------------------------------------
void do_save_if_pending()
{
    if ((SaveStatePending || SaveStateCoilLengthPending) &&
        ((HAL_GetTick() - LastKeyPushedTick) > 2000))
    {
        DBG_LOG("[STATE ] State Changed - Saving...\r\n");
        /* uint32_t _start_tick, _end_tick; */


        /* LED_BrightnessUp(); */
        /* LastKeyPushedTick = HAL_GetTick(); */

        if (SaveStatePending) {
            taskENTER_CRITICAL();
            /* _start_tick = HAL_GetTick(); */
            nv_save(&CurrentState, &DeviceConfig);
            /* _end_tick = HAL_GetTick(); */
            /* DBG_LOG("[%s] saving duration %d ms\r\n", __func__, _end_tick - _start_tick); */
            taskEXIT_CRITICAL();
        }

        if (DeviceConfig.RoomNumber == 1) {
            if (SaveStateCoilLengthPending) {
                taskENTER_CRITICAL();
                nv_save_coil_length(&CurrentState);
                taskEXIT_CRITICAL();
            }
        }

        SaveStatePending = false;
        SaveStateCoilLengthPending = false;
    }
}

#ifdef FEATURE_RAM_FUNCTION
#define gpio_write_pin(port, pin, set)   RAM_GPIO_WritePin(port, pin, set)
#else
#define gpio_write_pin(port, pin, set)   HAL_GPIO_WritePin(port, pin, set)
#endif

RAMFUNC void LED_Dimming()
{
    if (LED_RESV_DUTY > 0 && (led_duty_counter < LED_RESV_DUTY))
    {
        // LED ON
        gpio_write_pin(GPIOB, GPIO_PIN_2, GPIO_PIN_SET);
    }
    else
    {
        // LED OFF
        gpio_write_pin(GPIOB, GPIO_PIN_2, GPIO_PIN_RESET);
    }

    if ((LED_RED_DUTY) > 0 && (led_duty_counter < LED_RED_DUTY))
    {
        // LED ON
        gpio_write_pin(GPIOH, GPIO_PIN_0, GPIO_PIN_SET);
    }
    else
    {
        // LED OFF
        gpio_write_pin(GPIOH, GPIO_PIN_0, GPIO_PIN_RESET);
    }

    if ((LED_GREEN_DUTY) > 0 && (led_duty_counter < LED_GREEN_DUTY))
    {
        // LED ON
        gpio_write_pin(GPIOH, GPIO_PIN_1, GPIO_PIN_SET);
    }
    else
    {
        // LED OFF
        gpio_write_pin(GPIOH, GPIO_PIN_1, GPIO_PIN_RESET);
    }

    if (LED_BACKLIGHT_DUTY > 0 && (led_duty_counter < LED_BACKLIGHT_DUTY))
    {
        // LED_ ON
        gpio_write_pin(GPIOA, GPIO_PIN_11, GPIO_PIN_SET);
    }
    else
    {
        // LED OFF
        gpio_write_pin(GPIOA, GPIO_PIN_11, GPIO_PIN_RESET);
    }

    led_duty_counter ++;

    if (led_duty_counter == 100)
        led_duty_counter = 0;
}
