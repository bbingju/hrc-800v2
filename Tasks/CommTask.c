#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

#include "stm32l0xx_hal.h"
#include "cmsis_os.h"

#if defined ( __ICCARM__ )   /* IAR Compiler */
//#define FEATURE_RTT_FOR_DEBUG
#endif

#ifdef FEATURE_RTT_FOR_DEBUG
#include "SEGGER_RTT.h"
#endif  /* FEATURE_RTT_FOR_DEBUG */
#include "Common.h"
#include "UART_CircularBufferDMA.h"
#include "crc16.h"

#ifdef __cplusplus
extern "C"
{
#endif

    void TaskComm(void const *argument);

#ifdef __cplusplus
}
#endif

extern UART_HandleTypeDef hlpuart1;
extern UART_HandleTypeDef huart1;

__INLINE static int HandlePacket_PLC(uint8_t *buf, int len);

//------------------------------------------------------------------------------
static uint8_t uart1_rx_buf[256]; // DMA 버퍼

uint8_t COMM_RX_BUF[64]; // 패킷 버퍼
uint8_t RxBuffer[64];    // 세컨드 버퍼

int COMM_RX_BUF_POS = 0;

// uint8_t RTT_buffer[128];
// uint32_t RTT_buffer_pos;

char tx_packet[64];

UART_CircularBufferDMA UART1_DMA_BUFFER;

#define streq(a, b)          (!strncmp(a, b, strlen(b)))

//------------------------------------------------------------------------------
void TaskComm(void const *argument)
{
    UART_CircularBufferDMA_init(&UART1_DMA_BUFFER, &huart1, uart1_rx_buf, 256);

    // To-do: 현재는 DMA관련 인터럽트를 사용하고 있지 않기 때문에 버퍼가 넘치기 전에
    // 수시로 읽어줘야 함.
    // 버퍼 부족 및 IDLE 감지하여 처리할 수 있도록 향상시킬 필요가 있다.
    //
    // 아래 노트 참조
    // http://www.evernote.com/l/AD2tnsPnLuxKe79Z5EMCT8lqvRhLbpGkJtM/

    for (;;)
    {
        while (UART1_DMA_BUFFER.is_empty(&UART1_DMA_BUFFER) == false)
        {
            char c = (uint8_t)UART1_DMA_BUFFER.receive(&UART1_DMA_BUFFER);

            COMM_RX_BUF[COMM_RX_BUF_POS] = c;
            COMM_RX_BUF_POS++;

            if (c == '\n')
            {
                memset(RxBuffer, 0, sizeof(RxBuffer));
                memcpy(RxBuffer, COMM_RX_BUF, COMM_RX_BUF_POS);

                HandlePacket_PLC(RxBuffer, COMM_RX_BUF_POS);

                COMM_RX_BUF_POS = 0;
            }

            if (COMM_RX_BUF_POS >= sizeof(COMM_RX_BUF))
            {
                COMM_RX_BUF_POS = 0;
            }
        }
        __HAL_UART_CLEAR_OREFLAG(&huart1);

        // SEGGER RTT control message handling
#if 0
        while (SEGGER_RTT_HasData(0))
        {
            uint8_t temp_buf[8];
            uint32_t RTT_read_len = SEGGER_RTT_ReadNoLock(0, temp_buf, 1);

            if (RTT_read_len > 0)
            {
                RTT_buffer[RTT_buffer_pos] = temp_buf[0];
                RTT_buffer_pos++;

                if (temp_buf[0] == '\n')
                {
                    // new packet
                    RTT_buffer[RTT_buffer_pos] = '\0';
                    HandlePacket_PLC(RTT_buffer, RTT_buffer_pos);
                    RTT_buffer_pos = 0;
                    memset(RTT_buffer, 0, sizeof(RTT_buffer));
                }
                if (RTT_buffer_pos == sizeof(RTT_buffer))
                {
                    RTT_buffer_pos = 0;
                }
            }
        }
#endif
        osDelay(1);
    }
}

// 통신 프로토콜은 아래 노트 참조
// http://www.evernote.com/l/AD1ZEULoyZhG0Iov1WuHLQCrbGGhC4sB830/


static int source_addr;
static int dest_addr;

static char type[4] = {0, 0, 0, 0};
static char command[4] = {0, 0, 0, 0};

static char parameter[32];
static uint16_t crc16_rx_value;

static uint16_t encode_packet(char *buffer, int src, int dst, int type, const char *cmd, const char *param)
{
    char suffix[8] = { 0 };
    uint16_t buflen = 0;
    uint16_t crc_value = 0;

    if (!buffer)
        return -1;

    sprintf(buffer, "%u,%u,%s,%s,%s,", src, dst, type == 1 ? "REQ" : "RSP", cmd, param);
    buflen = strlen(buffer);
    crc_value = crc16_ccitt((uint8_t*)buffer, buflen);

    sprintf(buffer, "***+++,%u,%u,%s,%s,%s,", src, dst, type == 1 ? "REQ" : "RSP", cmd, param);
    sprintf(suffix, "%04X,\r\n", crc_value);
    strncat(buffer, suffix, 7);
    buflen = strlen(buffer);

    return buflen;
}

#define ENCODE_REQ_PACKET(buf, src, dst, cmd, param)        encode_packet(buf, src, dst, 1, cmd, param)
#define ENCODE_RSP_PACKET(buf, src, dst, cmd, param)        encode_packet(buf, src, dst, 0, cmd, param)

#define SEND_REQ_PACKET(buf, src, dst, cmd, param)       do {           \
        uint16_t retlen = ENCODE_REQ_PACKET(buf, src, dst, cmd, param); \
        if (retlen != -1) {                                             \
            DBG_LOG("[COMM    ] Request = %s", buf);                    \
            HAL_UART_Transmit(&huart1, (uint8_t *)buf, retlen, 100);    \
        }                                                               \
    } while(0);

#define SEND_RSP_PACKET(buf, src, dst, cmd, param)       do {           \
        uint16_t retlen = ENCODE_RSP_PACKET(buf, src, dst, cmd, param); \
        if ( retlen != -1) {                                            \
            DBG_LOG("[COMM    ] Response = %s", buf);                   \
            vTaskDelay(10);                                             \
            HAL_UART_Transmit(&huart1, (uint8_t *)buf, retlen, 100);    \
        }                                                               \
    } while(0);


static int scan_s(char *str, int str_len, int *s, int *e, const char delim)
{
    if (*e == str_len)
        return -1;
    for (*e = *s; *e <= str_len; *e += 1) {
        if ((str[*e] == delim) || (*e == str_len))
            return *e - *s;
    }
    return -2;
}

#ifdef FEATURE_OTHER_ROOMS_CONTROL
static int uda_modify_index = -1;
static float uda_modify_temp = 0.0f;
#endif  /* FEATURE_OTHER_ROOMS_CONTROL */

#define DEFINE_HANDLER(cmd)                                             \
    __INLINE static int handle_##cmd(uint8_t src, uint8_t dst,          \
                                     const char *type, const char *command, char *parameter)

#define HANDLE(cmd)                                             \
    if (!strncmp(command, #cmd, 3))                             \
        return handle_##cmd(source_addr, dest_addr, type,       \
                            command, parameter);

DEFINE_HANDLER(CLL)
{
    char param[32] = { 0 };
    for (int i = 0; i < 8; i++) {
        ROOM *r = &CurrentState.rooms[i];
        char item[8] = { 0 };
        sprintf(item, "%02u ", r->coil_length);
        item[3] = 0;
        strcat(param, item);
    }
    param[23] = 0;

    SEND_RSP_PACKET(tx_packet, DeviceConfig.RoomNumber, src, "CLL", param);

    return 0;
}

DEFINE_HANDLER(RCN)
{
    char param[8] = { 0 };
    sprintf(param, "%u", DeviceConfig.total_rooms);
    SEND_RSP_PACKET(tx_packet, DeviceConfig.RoomNumber, src, "RCN", param);
    return 0;
}

DEFINE_HANDLER(RSV)
{
    if (streq(type, "RSP")) {
        if (DeviceConfig.RoomNumber == 1 && src != 1) {
            ROOM *r = &CurrentState.rooms[src - 1];
            uint32_t block = 0;

            for (int i = 0; i < 24; i++) {
                block |= (((parameter[i] - '0') ? 1 : 0) << i);
            }

            if (r->reserved_time_block != block) {
                taskENTER_CRITICAL();
                r->reserved_time_block = block;
                taskEXIT_CRITICAL();
            }
        }
    }
    else {                      /* in case of REQ */
        if (DeviceConfig.RoomNumber == dst) {
            uint32_t block = CurrentState.rooms[dst - 1].reserved_time_block;
            char param[32] = { 0 };

            memset(tx_packet, 0, sizeof(tx_packet));
            for (int i = 0; i < 24; i++) {
                param[i] = (block & (1 << i)) ? '1' : '0';
            }
            SEND_RSP_PACKET(tx_packet, DeviceConfig.RoomNumber, src, "RSV", param);
        }
    }
    return 0;
}

DEFINE_HANDLER(STS)
{
    if (streq(type, "REQ")) {
        if (DeviceConfig.RoomNumber == dst) {
            ROOM *self_room = &CurrentState.rooms[dst - 1];
            char param[32] = { 0 };
            int intpart = (int)self_room->SettingTemperature;
            int decimalpart = (int)((self_room->SettingTemperature - intpart) * 10);
            int ct_intpart = (int)self_room->CurrentTemperature;
            int ct_decimalpart = (int) ((self_room->CurrentTemperature - ct_intpart) * 10);

            sprintf(param, "%02d.%01d %02d.%01d H%uR%uT%uL%uC%02u",
                    intpart, decimalpart, ct_intpart, ct_decimalpart,
                    self_room->heat_mode, self_room->reservation_mode_on,
                    CurrentState.set_time_just, CurrentState.leave_mode_on, self_room->coil_length);
            SEND_RSP_PACKET(tx_packet, dst, src, "STS", param);
            return 0;
        }
    }
#ifdef FEATURE_OTHER_ROOMS_CONTROL
    else {                      /* in case of RSP */
        if (DeviceConfig.RoomNumber == 1) {
            uint8_t to_update_number = src;
            if (to_update_number <= DeviceConfig.total_rooms && to_update_number != 0 &&
                DeviceConfig.RoomNumber != to_update_number)
            {
                ROOM *r = &CurrentState.rooms[to_update_number - 1];

                int start = 0, end = 0;
                int paramlen = strlen(parameter);
                float setting_temp = 0.0f, current_temp = 0.0f;
                heat_mode_t heat_mode = HEAT_MODE_OFF;
                bool reservation_mode_on = false;

                int ret = scan_s(parameter, paramlen, &start, &end, ' ');
                if (ret < 0)
                    return ret;

                char *tok = parameter + start;
                tok[end - start] = '\0';
                setting_temp = strtof(tok, NULL);

                start = end + 1;

                ret = scan_s(parameter, paramlen, &start, &end, ' ');
                if (ret < 0)
                    return ret;

                tok = parameter + start;
                tok[end - start] = '\0';
                current_temp = strtof(tok, NULL);

                start = end + 1;

                if (*tok == 'H')
                    heat_mode = *(tok + 1) - '0';
                if (*(tok + 2) == 'R')
                    reservation_mode_on = (*(tok + 3) - '0') > 0 ? true : false;

                if (to_update_number == uda_modify_index + 1 /* && */
                    /* (setting_temp == uda_modify_temp) *//*  || */
                     /* heat_mode == heat_mode_on_temp || */
                     /* reservation_mode_on == reservation_mode_on_temp) */) {

                    taskENTER_CRITICAL();
                    if (IS_MODIFIED_BIT_SET(CurrentState.modified_bits, uda_modify_index)) 
                        UNSET_MODIFIED_BIT(CurrentState.modified_bits, uda_modify_index);
                    else if (IS_MODIFIED_BIT_SET(CurrentState.modified_bits_heat_mode, uda_modify_index))
                        UNSET_MODIFIED_BIT(CurrentState.modified_bits_heat_mode, uda_modify_index);
                    taskEXIT_CRITICAL();
                    uda_modify_index = -1;
                }
                else {
                    taskENTER_CRITICAL();
                    r->CurrentTemperature = current_temp;
                    r->SettingTemperature = setting_temp;
                    /* r->heat_mode = heat_mode; */
                    /* r->reservation_mode_on = reservation_mode_on; */
                    taskEXIT_CRITICAL();
                }
            }
        }
    }
#endif  /* FEATURE_OTHER_ROOMS_CONTROL */
    return 0;
}

DEFINE_HANDLER(SVS)
{
    if (streq(type, "REQ") && dest_addr == DeviceConfig.RoomNumber)
    {
        ROOM *self = &CurrentState.rooms[DeviceConfig.RoomNumber - 1];
        float setting_temp         = self->SettingTemperature;
        heat_mode_t heat_mode      = self->heat_mode;
        bool reservation_mode_on   = self->reservation_mode_on;

        int start = 0, end = 0;
        int paramlen = strlen(parameter);
        int ret = scan_s(parameter, paramlen, &start, &end, ' ');
        if (ret < 0)
            return ret;

        char *tok = parameter + start;
        tok[end - start] = '\0';
        setting_temp = strtof(tok, NULL);
        start = end + 1;

        ret = scan_s(parameter, paramlen, &start, &end, ' ');
        if (ret < 0)
            return ret;

        tok = parameter + start;
        tok[end - start] = '\0';
        if (strlen(tok) >= 4) {
            if (*(tok + 0) == 'H')
                heat_mode = *(tok + 1) - '0';
            if (*(tok + 2) == 'R')
                reservation_mode_on = (*(tok + 3) - '0') > 0 ? true : false;
        }

        if (self->SettingTemperature != setting_temp ||
            self->heat_mode != heat_mode ||
            self->reservation_mode_on != reservation_mode_on) {
            taskENTER_CRITICAL();
            self->SettingTemperature = setting_temp;
            self->heat_mode = heat_mode;
            self->reservation_mode_on = reservation_mode_on;
            taskEXIT_CRITICAL();
            ReserveSaveState();
            taskYIELD();
        }
    }
    return 0;
}

DEFINE_HANDLER(TSN)
{
    int hour = (parameter[0] - '0') * 10 + (parameter[1] - '0');
    int min = (parameter[2] - '0') * 10 + (parameter[3] - '0');

    if ((hour >= 0 && hour < 24) && (min >= 0 && min < 60)) {
        taskENTER_CRITICAL();
        CurrentState.current_time.hour = hour;
        CurrentState.current_time.min  = min;
        taskEXIT_CRITICAL();
    }

    char param[24] = { 0 };
    sprintf(param, "%02u%02u", CurrentState.current_time.hour, CurrentState.current_time.min);
    SEND_RSP_PACKET(tx_packet, DeviceConfig.RoomNumber, src, "TSN", param);

    return 0;
}

DEFINE_HANDLER(TST)
{
    if (DeviceConfig.RoomNumber == 1) {
        char param[24] = { 0 };
        sprintf(param, "%02u%02u", CurrentState.current_time.hour, CurrentState.current_time.min);
        SEND_RSP_PACKET(tx_packet, DeviceConfig.RoomNumber, src, "TST", param);
    }
    return 0;
}

DEFINE_HANDLER(TSY)
{
    if (CurrentState.set_time_just) {
        taskENTER_CRITICAL();
        CurrentState.set_time_just = 0;
        taskEXIT_CRITICAL();
    }

    return 0;
}

DEFINE_HANDLER(UDA)
{
#ifdef FEATURE_OTHER_ROOMS_CONTROL
    if (DeviceConfig.RoomNumber == 1)
    {
        bool found = false;
        int i = 0;

        for (i = 0; i < DeviceConfig.total_rooms; i++) {
            if (IS_MODIFIED_BIT_SET(CurrentState.modified_bits, i)) {
                uda_modify_index = i;
                found = true;
                break;
            }

            if (IS_MODIFIED_BIT_SET(CurrentState.modified_bits_heat_mode, i)) {
                uda_modify_index = i;
                found = true;
                break;
            }
        }

        if (found) {
            char param[32] = { 0 };
            ROOM *modified_room = &CurrentState.rooms[i];
            uda_modify_temp = modified_room->SettingTemperature;
            int intpart = (int)uda_modify_temp;
            int decimalpart = (int) ((uda_modify_temp - intpart) * 10);

            sprintf(param, "%02d.%01d H%uR%u", intpart, decimalpart,
                    modified_room->heat_mode ? HEAT_MODE_ON : HEAT_MODE_OFF,
                    modified_room->reservation_mode_on);
            /* for (int j = 0; j < 3; j++) */
            SEND_REQ_PACKET(tx_packet, DeviceConfig.RoomNumber, uda_modify_index + 1, "SVS", param);

            /* taskENTER_CRITICAL(); */
            /* UNSET_MODIFIED_BIT(CurrentState.modified_bits, uda_modify_index); */
            /* taskEXIT_CRITICAL(); */
        }
    }
#endif  /* FEATURE_OTHER_ROOMS_CONTROL */
    return 0;
}

DEFINE_HANDLER(UDP)
{
    if (streq(type, "RSP")) {
        if (DeviceConfig.RoomNumber == 1 && src != 1) {
            ROOM *r = &CurrentState.rooms[src - 1];
            heat_mode_t heat_mode   = r->heat_mode;

            heat_mode = parameter[1] - '0';

            if (r->heat_mode != heat_mode) {
                taskENTER_CRITICAL();
                r->heat_mode = heat_mode;
                taskEXIT_CRITICAL();
            }
        }
    }
    else {
        if (DeviceConfig.RoomNumber == dst) {
            ROOM *self = &CurrentState.rooms[DeviceConfig.RoomNumber - 1];

            int start = 0, end = 0;
            int paramlen = strlen(parameter);
            heat_mode_t heat_mode = HEAT_MODE_OFF;
            bool leave_on = false;
            bool set_temp_or_not = false;
            float setting_temp = 0.0f;

            char param[24]        = { 0 };
            bool changed = false;

            int ret = scan_s(parameter, paramlen, &start, &end, ' ');
            if (ret < 0)
                return ret;

            char *tok = parameter + start;
            tok[end - start] = '\0';

            for (int i = 0; i < end; i += 2) {
                if (*(tok + i) == 'H') {
                    heat_mode = *(tok + i + 1) - '0';
                }
                else if (*(tok + i) == 'L') {
                    leave_on = (*(tok + i + 1) - '0') > 0 ? true : false;
                    /* leave_on = true; */
                }
                else if (*(tok + i) == 'T') {
                    set_temp_or_not = (*(tok + i + 1) - '0') > 0 ? true : false;
                }
            }

            if (self->heat_mode != heat_mode) {
                /* DBG_LOG("heat mode changed by UDP!!! old(%u), new(%u)\r\n", self->heat_mode, heat_mode); */

                if (IS_MODIFIED_BIT_SET(CurrentState.modified_bits_heat_mode, dst - 1)) {
                    /* DBG_LOG("A transaction is not completed\r\n"); */
                    taskENTER_CRITICAL();
                    UNSET_MODIFIED_BIT(CurrentState.modified_bits_heat_mode, dst - 1);
                    taskEXIT_CRITICAL();
                    return -1;
                }

                taskENTER_CRITICAL();
                self->heat_mode = heat_mode;
                taskEXIT_CRITICAL();
                changed = true;
            }

            if (CurrentState.leave_mode_on != leave_on) {
                /* DBG_LOG("leave mode changed by UDP!!! old(%u), new(%u)\r\n", */
                /*         CurrentState.leave_mode_on, leave_on); */
                taskENTER_CRITICAL();
                CurrentState.leave_mode_on = leave_on;
                taskEXIT_CRITICAL();
                changed = true;
            }

            start = end + 1;

            ret = scan_s(parameter, paramlen, &start, &end, ' ');
            if (ret < 0)
                goto _before_reserve_saving;

            tok = parameter + start;
            tok[end - start] = '\0';
            setting_temp = strtof(tok, NULL);

            start = end + 1;

            if (set_temp_or_not) {
                taskENTER_CRITICAL();
                self->SettingTemperature = setting_temp;
                taskEXIT_CRITICAL();
                changed = true;
            }

            /* int intpart = (int)setting_temp; */
            /* int decimalpart = (int) ((setting_temp - intpart) * 10); */

            sprintf(param, "H%01uL%01uT%01u %s", self->heat_mode, CurrentState.leave_mode_on, set_temp_or_not, tok);
            SEND_RSP_PACKET(tx_packet, DeviceConfig.RoomNumber, source_addr, "UDP", param);

        _before_reserve_saving:
            if (changed)
                ReserveSaveState();
        }
    }

    return 0;
}

__INLINE static int HandlePacket_PLC(uint8_t *buf, int len)
{
    if (buf[0] != '*')
        return -1;

    /* DBG_LOG("[COMM    ] Rx Packet = %s", buf); */

    uint16_t crc16_calc_value = crc16_ccitt(buf, len - 7);

    memset(parameter, 0, sizeof(parameter));

    char *ptr;


#if defined(__GNUC__)
    char *saveptr;
    ptr = strtok_r((char *)buf, ",", &saveptr);
    ptr = strtok_r(NULL, ",", &saveptr); // Source Address
    source_addr = atoi(ptr);
    ptr = strtok_r(NULL, ",", &saveptr); // Destination Address
    dest_addr = atoi(ptr);
    ptr = strtok_r(NULL, ",", &saveptr); // Type
    strncpy(type, ptr, 3);
    ptr = strtok_r(NULL, ",", &saveptr); // Command
    strncpy(command, ptr, 3);
    ptr = strtok_r(NULL, ",", &saveptr); // Parameter
    strncpy(parameter, ptr, 32);
    ptr = strtok_r(NULL, ",", &saveptr); // CRC16
    crc16_rx_value = strtol(ptr, NULL, 16);
    ptr = strtok_r(NULL, ",", &saveptr); // End '\n' or '\r\n'
#else
    taskENTER_CRITICAL();
    ptr = strtok((char *)buf, ",");
    ptr = strtok(NULL, ",");    // Source Address
    source_addr = atoi(ptr);
    ptr = strtok(NULL, ",");    // Destination Address
    dest_addr = atoi(ptr);
    ptr = strtok(NULL, ",");    // Type
    strncpy(type, ptr, 3);
    ptr = strtok(NULL, ",");    // Command
    strncpy(command, ptr, 3);
    ptr = strtok(NULL, ",");    // Parameter
    strncpy(parameter, ptr, 32);
    ptr = strtok(NULL, ",");    // CRC16
    crc16_rx_value = strtol(ptr, NULL, 16);
    ptr = strtok(NULL, ",");    // End '\n' or '\r\n'
    taskEXIT_CRITICAL();
#endif

    if (streq(type, "RSP") && DeviceConfig.RoomNumber == source_addr) {
        return -2;
    }

    if (dest_addr != DeviceConfig.RoomNumber)
    {
        // Destination Address가 자기 것이 아닌 경우 패킷 무시함.
        // 단 "SVS" 명령의 경우 조절기가 다른 방의 온도값을 모두 알고 있어야 하기 때문에 예외적으로 처리해줌.
        if (strncmp(command, "SVS", 3) && strncmp(command, "RSV", 3) &&
            strncmp(command, "STS", 3) && strncmp(command, "UDP", 3))
            return -3;
    }

    DBG_LOG("[COMM    ] %u--->%u, Type = %s, Command = %s, Parameter = %s, "
                      "CRC16(Received) = %04X, CRC16(Calcualted) = %04X\r\n",
                      source_addr, dest_addr,
                      type, command, parameter,
                      crc16_rx_value, crc16_calc_value);

    if (crc16_calc_value != crc16_rx_value) {
        DBG_LOG("[COMM    ] CRC16 Invalid! "
                          "calculated (%04X), received (%04X). Discarding this packet.\r\n",
                          crc16_calc_value, crc16_rx_value);
        return -4;
    }

    // 명령어 처리
    if (streq(type, "RSP")) // "REQ" 타입이 아닌 경우 리턴. 조절기는 REQ 타입만 처리함.
    {
        if (strncmp(command, "SVS", 3) && strncmp(command, "RSV", 3) &&
            strncmp(command, "STS", 3) && strncmp(command, "UDP", 3))
            return -2;
    }

    HANDLE(CLL);
    HANDLE(RCN);
    HANDLE(RSV);
    HANDLE(STS);
    HANDLE(SVS);
    HANDLE(TSN);                /* Time Sync 패킷 */
    HANDLE(TST);
    HANDLE(TSY);
    HANDLE(UDA);                /* 1번에게 제어권 부여 */
    HANDLE(UDP);

    return 0;
}
