#ifndef __UART_CIRCULAR_BUFFER_DMA_H_
#define __UART_CIRCULAR_BUFFER_DMA_H_

#include <stdint.h>
#include <stdbool.h>
#include "stm32l0xx_hal.h"

// UART DMA Circular Buffer for STM32

typedef struct _UART_CircularBufferDMA
{
    UART_HandleTypeDef *handle;
    uint32_t   ReadPtr;
    uint32_t   BufferSize;
    uint8_t    *Buffer;

    bool (*is_empty)(struct _UART_CircularBufferDMA *);
    int  (*receive)(struct _UART_CircularBufferDMA *);
} UART_CircularBufferDMA;

int UART_CircularBufferDMA_init(UART_CircularBufferDMA *obj,
                                UART_HandleTypeDef *handle,
                                uint8_t* _Buffer,
                                uint32_t _BufferSize);

// class UART_CircularBufferDMA
// {
//  private:
//      UART_HandleTypeDef *UART_Object;

//      uint32_t   ReadPtr;
//      uint32_t   BufferSize;
//      uint8_t    *Buffer;

//      inline uint32_t GetDMA_WritePTR()
//      {
//          // NDTR = Number of Data 
//          return ((BufferSize - UART_Object->hdmarx->Instance->CNDTR) & (BufferSize - 1));
//      }

//  public:

//      UART_CircularBufferDMA()
//      {
//          BufferSize = 0;
//      }

//      void Init(UART_HandleTypeDef *huart, uint8_t* _Buffer, uint32_t _BufferSize)
//      {
//          UART_Object = huart;
//          ReadPtr = 0;
//          Buffer = _Buffer;
//          BufferSize = _BufferSize;

//          HAL_UART_Receive_DMA(UART_Object, Buffer, BufferSize);
//      }

//      bool isEmpty(void)
//      {
//          if (ReadPtr == GetDMA_WritePTR())
//          {
//              return true;
//          }
//          return false;
//      }

//      int Receive(void)
//      {
//          uint8_t c = 0;
//          if (ReadPtr != GetDMA_WritePTR())
//          {
//              c = Buffer[ReadPtr++];
//              ReadPtr &= (BufferSize - 1);
//              return c;
//          }
//          else
//          {
//              return -1;
//          }
//      }
// };

#endif
