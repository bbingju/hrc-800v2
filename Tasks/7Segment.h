#pragma once

#include <stdint.h>

/* refer to IAR Technical Note 46979,
 * https://www.iar.com/support/tech-notes/compiler/any-binary-notation-in-c-source/ */
#define b0000000      0x00
#define b0000001      0x01
#define b0000010      0x02
#define b0000100      0x04
#define b0000101      0x05
#define b0001000      0x08
#define b0001101      0x0d
#define b0001110      0x0e
#define b0001111      0x0f
#define b0010000      0x10
#define b0010101      0x15
#define b0010111      0x17
#define b0011100      0x1c
#define b0011101      0x1d
#define b0011111      0x1f
#define b0100000      0x20
#define b0100010      0x22
#define b0110000      0x30
#define b0110011      0x33
#define b0110111      0x37
#define b0111000      0x38
#define b0111011      0x3b
#define b0111101      0x3d
#define b0111110      0x3e
#define b1000111      0x47
#define b1001110      0x4e
#define b1001111      0x4f
#define b1011011      0x5b
#define b1011110      0x5e
#define b1011111      0x5f
#define b1100111      0x67
#define b1101101      0x6d
#define b1101111      0x6f
#define b1110000      0x70
#define b1110011      0x73
#define b1110111      0x77
#define b1111000      0x78
#define b1111001      0x79
#define b1111011      0x7b
#define b1111110      0x7e
#define b1111111      0x7f

static const uint8_t SegmentLED_CharMap[] = 
{
  /* ABCDEFG  Segments      7-segment map: */
  b1111110, // 0   "0"          AAA
  b0110000, // 1   "1"         F   B
  b1101101, // 2   "2"         F   B
  b1111001, // 3   "3"          GGG
  b0110011, // 4   "4"         E   C
  b1011011, // 5   "5"         E   C
  b1011111, // 6   "6"          DDD
  b1110000, // 7   "7"
  b1111111, // 8   "8"
  b1111011, // 9   "9"
  b1110111, // 10  "A"
  b0011111, // 11  "b"
  b1001110, // 12  "C"
  b0111101, // 13  "d"
  b1001111, // 14  "E"
  b1000111, // 15  "F"
  b0000000, // 16  NO DISPLAY
  b0000000, // 17  NO DISPLAY
  b0000000, // 18  NO DISPLAY
  b0000000, // 19  NO DISPLAY
  b0000000, // 20  NO DISPLAY
  b0000000, // 21  NO DISPLAY
  b0000000, // 22  NO DISPLAY
  b0000000, // 23  NO DISPLAY
  b0000000, // 24  NO DISPLAY
  b0000000, // 25  NO DISPLAY
  b0000000, // 26  NO DISPLAY
  b0000000, // 27  NO DISPLAY
  b0000000, // 28  NO DISPLAY
  b0000000, // 29  NO DISPLAY
  b0000000, // 30  NO DISPLAY
  b0000000, // 31  NO DISPLAY
  b0000000, // 32  ' '
  b0000000, // 33  '!'  NO DISPLAY
  b0100010, // 34  '"'
  b0000000, // 35  '#'  NO DISPLAY
  b0000000, // 36  '$'  NO DISPLAY
  b0000000, // 37  '%'  NO DISPLAY
  b0000000, // 38  '&'  NO DISPLAY
  b0100000, // 39  '''
  b1001110, // 40  '('
  b1111000, // 41  ')'
  b0000000, // 42  '*'  NO DISPLAY
  b0000000, // 43  '+'  NO DISPLAY
  b0000100, // 44  ','
  b0000001, // 45  '-'
  b0000000, // 46  '.'  NO DISPLAY
  b0000000, // 47  '/'  NO DISPLAY
  b1111110, // 48  '0'
  b0110000, // 49  '1'
  b1101101, // 50  '2'
  b1111001, // 51  '3'
  b0110011, // 52  '4'
  b1011011, // 53  '5'
  b1011111, // 54  '6'
  b1110000, // 55  '7'
  b1111111, // 56  '8'
  b1111011, // 57  '9'
  b0000000, // 58  ':'  NO DISPLAY
  b0000000, // 59  ';'  NO DISPLAY
  b0000000, // 60  '<'  NO DISPLAY
  b0000000, // 61  '='  NO DISPLAY
  b0000000, // 62  '>'  NO DISPLAY
  b0000000, // 63  '?'  NO DISPLAY
  b0000000, // 64  '@'  NO DISPLAY
  b1110111, // 65  'A'
  b0011111, // 66  'b'
  b1001110, // 67  'C'
  b0111101, // 68  'd'
  b1001111, // 69  'E'
  b1000111, // 70  'F'
  b1011110, // 71  'G'
  b0110111, // 72  'H'
  b0110000, // 73  'I'
  b0111000, // 74  'J'
  b0000000, // 75  'K'  NO DISPLAY
  b0001110, // 76  'L'
  b0000000, // 77  'M'  NO DISPLAY
  b0010101, // 78  'n'
  b1111110, // 79  'O'
  b1100111, // 80  'P'
  b1110011, // 81  'q'
  b0000101, // 82  'r'
  b1011011, // 83  'S'
  b0001111, // 84  't'
  b0111110, // 85  'U'
  b0000000, // 86  'V'  NO DISPLAY
  b0000000, // 87  'W'  NO DISPLAY
  b0000000, // 88  'X'  NO DISPLAY
  b0111011, // 89  'y'
  b0000000, // 90  'Z'  NO DISPLAY
  b1001110, // 91  '['
  b0000000, // 92  '\'  NO DISPLAY
  b1111000, // 93  ']'
  b0000000, // 94  '^'  NO DISPLAY
  b0001000, // 95  '_'
  b0000010, // 96  '`'
  b1110111, // 97  'a' SAME AS CAP
  b0011111, // 98  'b' SAME AS CAP
  b0001101, // 99  'c'
  b0111101, // 100 'd' SAME AS CAP
  b1101111, // 101 'e'
  b1000111, // 102 'F' SAME AS CAP
  b1011110, // 103 'G' SAME AS CAP
  b0010111, // 104 'h'
  b0010000, // 105 'i'
  b0111000, // 106 'j' SAME AS CAP
  b0000000, // 107 'k'  NO DISPLAY
  b0110000, // 108 'l'
  b0000000, // 109 'm'  NO DISPLAY
  b0010101, // 110 'n' SAME AS CAP
  b0011101, // 111 'o'
  b1100111, // 112 'p' SAME AS CAP
  b1110011, // 113 'q' SAME AS CAP
  b0000101, // 114 'r' SAME AS CAP
  b1011011, // 115 'S' SAME AS CAP
  b0001111, // 116 't' SAME AS CAP
  b0011100, // 117 'u'
  b0000000, // 118 'b'  NO DISPLAY
  b0000000, // 119 'w'  NO DISPLAY
  b0000000, // 120 'x'  NO DISPLAY
  b0000000, // 121 'y'  NO DISPLAY
  b0000000, // 122 'z'  NO DISPLAY
  b0000000, // 123 '0b'  NO DISPLAY
  b0000000, // 124 '|'  NO DISPLAY
  b0000000, // 125 ','  NO DISPLAY
  b0000000, // 126 '~'  NO DISPLAY
  b0000000, // 127 'DEL'  NO DISPLAY
};
