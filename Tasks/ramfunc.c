/**
  ******************************************************************************
  * @file    EEPROM-ComWhileWrite/src/ramfunc.c
  * @author  MCD Application Team
  * @version V0.1.0
  * @date    16th February 2016
  * @brief   This file provides all the memory related operation functions.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2016 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/** @addtogroup EEPROM-ComWhileWrite
  * @{
  */

/* Includes ------------------------------------------------------------------*/
#include "ramfunc.h"


/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
uint8_t   aDataBuffer[8];
uint32_t  aNVOffset = 0;
uint32_t  bWritten;
extern uint32_t uwTick;

/* Private function prototypes -----------------------------------------------*/
RAMFUNC uint32_t RAM_GetTick(void);
RAMFUNC static void RAM_FLASH_SetErrorCode(void);
RAMFUNC HAL_StatusTypeDef RAM_FLASH_WaitForLastOperation(uint32_t timeout);
/* RAMFUNC static void RAM_UART_DMAReceiveCplt(DMA_HandleTypeDef *hdma); */
/* RAMFUNC static void RAM_DMA_SetConfig(DMA_HandleTypeDef *hdma, uint32_t src_address, uint32_t dst_address, uint32_t data_length); */
/* RAMFUNC HAL_StatusTypeDef RAM_DMA_Start_IT(DMA_HandleTypeDef *hdma, uint32_t src_address, uint32_t dst_address, uint32_t data_length); */

/* Private functions ---------------------------------------------------------*/

/**
  * @brief Returns tick value
  * @retval system tick for timeout purposes
  */
RAMFUNC uint32_t RAM_GetTick(void)
{
    return uwTick;
  //return pubTick;
}

#ifdef USE_STM32L152_EVAL
#define ProcFlash pFlash
#endif
/**
  * @brief  Set the specific FLASH error flag.
  * @retval None
  */
RAMFUNC __INLINE static void RAM_FLASH_SetErrorCode(void)
{
  if (__HAL_FLASH_GET_FLAG(FLASH_FLAG_WRPERR))
  {
    pFlash.ErrorCode |= HAL_FLASH_ERROR_WRP;
    __HAL_FLASH_CLEAR_FLAG( FLASH_FLAG_WRPERR );
  }
  if (__HAL_FLASH_GET_FLAG(FLASH_FLAG_PGAERR))
  {
    pFlash.ErrorCode |= HAL_FLASH_ERROR_PGA;
    __HAL_FLASH_CLEAR_FLAG( FLASH_FLAG_PGAERR );
  }
  if (__HAL_FLASH_GET_FLAG(FLASH_FLAG_SIZERR))
  {
    pFlash.ErrorCode |= HAL_FLASH_ERROR_SIZE;
    __HAL_FLASH_CLEAR_FLAG( FLASH_FLAG_SIZERR );
  }
  if (__HAL_FLASH_GET_FLAG(FLASH_FLAG_OPTVERR))
  {
    pFlash.ErrorCode |= HAL_FLASH_ERROR_OPTV;
    __HAL_FLASH_CLEAR_FLAG( FLASH_FLAG_OPTVERR );
  }
#if defined(FLASH_SR_RDERR)
  if (__HAL_FLASH_GET_FLAG(FLASH_FLAG_RDERR))
  {
    pFlash.ErrorCode |= HAL_FLASH_ERROR_RD;
    __HAL_FLASH_CLEAR_FLAG( FLASH_FLAG_RDERR );
  }
#endif
#if defined(FLASH_FLAG_FWWERR)
  if (__HAL_FLASH_GET_FLAG(FLASH_FLAG_FWWERR))
  {
    pFlash.ErrorCode |= HAL_FLASH_ERROR_FWWERR;
    __HAL_FLASH_CLEAR_FLAG( FLASH_FLAG_FWWERR );
  }
#endif
#if defined(FLASH_FLAG_NOTZEROERR)
  if (__HAL_FLASH_GET_FLAG(FLASH_FLAG_NOTZEROERR))
  {
    pFlash.ErrorCode |= HAL_FLASH_ERROR_NOTZERO;
    __HAL_FLASH_CLEAR_FLAG( FLASH_FLAG_NOTZEROERR );
  }
#endif
}

/**
  * @brief  Wait for a FLASH operation to complete.
  * @param  timeout: maximum flash operationtimeout
  * @retval HAL status
  */
RAMFUNC __INLINE static HAL_StatusTypeDef RAM_FLASH_WaitForLastOperation(uint32_t Timeout)
{
    /* Wait for the FLASH operation to complete by polling on BUSY flag to be reset.
       Even if the FLASH operation fails, the BUSY flag will be reset and an error
       flag will be set */
     
    uint32_t tickstart = RAM_GetTick();
     
    while(__HAL_FLASH_GET_FLAG(FLASH_FLAG_BSY))
    {
        if (Timeout != HAL_MAX_DELAY)
        {
            if((Timeout == 0U) || ((RAM_GetTick()-tickstart) > Timeout))
            {
                return HAL_TIMEOUT;
            }
        }
    }
  
    /* Check FLASH End of Operation flag  */
    if (__HAL_FLASH_GET_FLAG(FLASH_FLAG_EOP))
    {
        /* Clear FLASH End of Operation pending bit */
        __HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_EOP);
    }
  
    if( __HAL_FLASH_GET_FLAG(FLASH_FLAG_WRPERR)     ||
        __HAL_FLASH_GET_FLAG(FLASH_FLAG_PGAERR)     ||
        __HAL_FLASH_GET_FLAG(FLASH_FLAG_SIZERR)     ||
        __HAL_FLASH_GET_FLAG(FLASH_FLAG_OPTVERR)    ||
        __HAL_FLASH_GET_FLAG(FLASH_FLAG_RDERR)      ||
        __HAL_FLASH_GET_FLAG(FLASH_FLAG_FWWERR)     ||
        __HAL_FLASH_GET_FLAG(FLASH_FLAG_NOTZEROERR) )
    {
        /*Save the error code*/

        /* WARNING : On the first cut of STM32L031xx and STM32L041xx devices,
         *           (RefID = 0x1000) the FLASH_FLAG_OPTVERR bit was not behaving
         *           as expected. If the user run an application using the first
         *           cut of the STM32L031xx device or the first cut of the STM32L041xx
         *           device, this error should be ignored. The revId of the device
         *           can be retrieved via the HAL_GetREVID() function.
         *
         */
        RAM_FLASH_SetErrorCode();
        return HAL_ERROR;
    }

    /* There is no error flag set */
    return HAL_OK;
}

/* RAMFUNC __INLINE static HAL_StatusTypeDef RAM_FLASH_WaitForLastOperation(uint32_t timeout) */
/* { */
/*     uint32_t  tic_time; */
/*     HAL_StatusTypeDef result = HAL_OK; */
/*     /\* Wait for the FLASH operation to complete by polling on BUSY flag to be reset. */
/*        Even if the FLASH operation fails, the BUSY flag will be reset and an error */
/*        flag will be set *\/ */

/*     uint32_t tickstart = RAM_GetTick(); */

/*     while (__HAL_FLASH_GET_FLAG(FLASH_FLAG_BSY) != RESET) */
/*     { */
/*         if (timeout != HAL_MAX_DELAY) */
/*         { */
/*             tic_time = RAM_GetTick() - tickstart; */
/*             if ((timeout == 0) || (tic_time > timeout)) */
/*             { */
/*                 result = HAL_TIMEOUT; */
/*             } */
/*         } */
/*     } */

/*     if ( result == HAL_OK ) */
/*     { */
/*         /\* Check FLASH End of Operation flag  *\/ */
/*         if (__HAL_FLASH_GET_FLAG(FLASH_FLAG_EOP)) */
/*         { */
/*             /\* Clear FLASH End of Operation pending bit *\/ */
/*             __HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_EOP); */
/*         } */

/*         if ( (FLASH->SR & (FLASH_FLAG_WRPERR | FLASH_FLAG_SIZERR | FLASH_FLAG_OPTVERR | FLASH_FLAG_PGAERR )) */
/*              != RESET) */
/*         { */
/*             /\* Save the error code *\/ */
/*             RAM_FLASH_SetErrorCode(); */
/*             result = HAL_ERROR; */
/*         } */
/*     } */

/*     /\* There is no error flag set *\/ */
/*     return result; */
/* } */

/**
  * @brief  Program word at a specified address
  * @param  address:  specifies the address to be programmed.
  * @param  data: specifies the data to be programmed
  *
  * @retval HAL_StatusTypeDef HAL Status
  */
RAMFUNC HAL_StatusTypeDef RAM_FLASHEx_DATAEEPROM_Program(uint32_t address, uint32_t data)
{
    HAL_StatusTypeDef status = HAL_ERROR;
    /* Process Locked */
    __HAL_LOCK(&pFlash);

    /* Check the parameters */
    assert_param(IS_FLASH_DATA_ADDRESS(address));

    /* Wait for last operation to be completed */
    status = RAM_FLASH_WaitForLastOperation(4);

    if (status == HAL_OK) {
        /* Clean the error context */
        pFlash.ErrorCode = HAL_FLASH_ERROR_NONE;

        /* Program word (32-bit) at a specified address */
        *(__IO uint32_t *)address = data;

        /* if (status != HAL_OK) { */
        /*     /\* Wait for last operation to be completed *\/ */
        /*     status = RAM_FLASH_WaitForLastOperation(4); */
        /* } */
    }

    /* Process Unlocked */
    __HAL_UNLOCK(&pFlash);
    return HAL_OK;
}

/* /\** */
/*   * @brief Rx Transfer completed callbacks */
/*   * @param huart: uart handle */
/*   * @retval None */
/*   *\/ */
/* RAMFUNC void RAM_UART_RxCpltCallback(UART_HandleTypeDef *huart) */
/* { */
/*   HAL_StatusTypeDef status; */
/*   uint32_t itn; */

/*   itn = *(uint32_t*) & (aDataBuffer[(aNVOffset&7)]); */

/*   status = RAM_FLASHEx_DATAEEPROM_Program( DATA_ADDRESS + aNVOffset, itn ); */

/*   if ( status != HAL_OK) */
/*   { */
/*     /\* An error occurred while writing to Flash memory *\/ */
/*     /\* End session - calling function in program Flash is no problem now *\/ */
/*     Serial_PutByte(CA); */
/*     Serial_PutByte(CA); */
/*   } */
/*   else */
/*   { */
/*     bWritten = 1; */
/*   } */
/* } */

/* /\** */
/*   * @brief DMA UART receive process complete callback */
/*   * @param hdma: DMA handle */
/*   * @retval None */
/*   *\/ */
/* RAMFUNC static void RAM_UART_DMAReceiveCplt(DMA_HandleTypeDef *hdma) */
/* { */
/*   UART_HandleTypeDef* huart = ( UART_HandleTypeDef* )((DMA_HandleTypeDef* )hdma)->Parent; */

/*   /\* DMA Normal mode*\/ */
/*   if ((hdma->Instance->CCR & DMA_CCR_CIRC) == 0) */
/*   { */
/*     huart->RxXferCount = 0; */

/*     /\* Disable the DMA transfer for the receiver request by setting the DMAR bit */
/*     in the UART CR3 register *\/ */
/*     huart->Instance->CR3 &= (uint32_t)~((uint32_t)USART_CR3_DMAR); */

/*     /\* Check if a transmit Process is ongoing or not *\/ */
/*     if (huart->State == HAL_UART_STATE_BUSY_TX_RX) */
/*     { */
/*       huart->State = HAL_UART_STATE_BUSY_TX; */
/*     } */
/*     else */
/*     { */
/*       huart->State = HAL_UART_STATE_READY; */
/*     } */
/*   } */
/*   RAM_UART_RxCpltCallback(huart); */
/* } */


/* /\* */
/* * @brief  Sets the DMA Transfer parameter. */
/* * @param  hdma:       pointer to a DMA_HandleTypeDef structure that contains */
/* *                     the configuration information for the specified DMA Channel.   */
/* * @param  src_address: The source memory Buffer address */
/* * @param  dst_address: The destination memory Buffer address */
/* * @param  data_length: The length of data to be transferred from source to destination */
/* * @retval HAL status */
/* *\/ */
/* RAMFUNC static void RAM_DMA_SetConfig(DMA_HandleTypeDef *hdma, uint32_t src_address, uint32_t dst_address, uint32_t data_length) */
/* { */
/*   /\* Configure DMA Channel data length *\/ */
/*   hdma->Instance->CNDTR = data_length; */

/*   /\* Peripheral to Memory *\/ */
/*   if ((hdma->Init.Direction) == DMA_MEMORY_TO_PERIPH) */
/*   { */
/*     /\* Configure DMA Channel destination address *\/ */
/*     hdma->Instance->CPAR = dst_address; */

/*     /\* Configure DMA Channel source address *\/ */
/*     hdma->Instance->CMAR = src_address; */
/*   } */
/*   /\* Memory to Peripheral *\/ */
/*   else */
/*   { */
/*     /\* Configure DMA Channel source address *\/ */
/*     hdma->Instance->CPAR = src_address; */

/*     /\* Configure DMA Channel destination address *\/ */
/*     hdma->Instance->CMAR = dst_address; */
/*   } */
/* } */
/* /\** */
/*   * @} */
/*   *\/ */

/* /\** */
/*   * @brief  Start the DMA Transfer with interrupt enabled. */
/*   * @param  hdma:       pointer to a DMA_HandleTypeDef structure that contains */
/*   *                     the configuration information for the specified DMA Channel. */
/*   * @param  src_address: The source memory Buffer address */
/*   * @param  dst_address: The destination memory Buffer address */
/*   * @param  data_length: The length of data to be transferred from source to destination */
/*   * @retval HAL status */
/*   *\/ */
/* RAMFUNC HAL_StatusTypeDef RAM_DMA_Start_IT(DMA_HandleTypeDef *hdma, uint32_t src_address, uint32_t dst_address, uint32_t data_length) */
/* { */
/*   /\* Process locked *\/ */
/*   __HAL_LOCK(hdma); */

/*   /\* Change DMA peripheral state *\/ */
/*   hdma->State = HAL_DMA_STATE_BUSY; */

/*   /\* Check the parameters *\/ */
/*   assert_param(IS_DMA_BUFFER_SIZE(data_length)); */

/*   /\* Disable the peripheral *\/ */
/*   __HAL_DMA_DISABLE(hdma); */

/*   /\* Configure the source, destination address and the data length *\/ */
/*   RAM_DMA_SetConfig(hdma, src_address, dst_address, data_length); */

/*   /\* Enable the transfer complete interrupt *\/ */
/*   __HAL_DMA_ENABLE_IT(hdma, DMA_IT_TC); */

/*   /\* Enable the transfer Error interrupt *\/ */
/*   __HAL_DMA_ENABLE_IT(hdma, DMA_IT_TE); */

/*   /\* Enable the Peripheral *\/ */
/*   __HAL_DMA_ENABLE(hdma); */

/*   return HAL_OK; */
/* } */

/* /\* Public functions ---------------------------------------------------------*\/ */

/* /\** */
/*   * @brief  Receive a packet from sender */
/*   * @param  length */
/*   *     0: end of transmission */
/*   *     2: abort by sender */
/*   *    >0: packet length */
/*   * @retval HAL_OK: normally return */
/*   *         HAL_BUSY: abort by user */
/*   *\/ */
/* RAMFUNC HAL_StatusTypeDef RAMReceivePacket( uint32_t length ) */
/* { */
/*   HAL_StatusTypeDef status = HAL_OK; */
/*   uint32_t block_size = 0; */
/*   uint32_t  tic_time, tickstart; */

/*   while ((status == HAL_OK) /\*&& (offset < f_size)*\/ && (block_size < length)) */
/*   { */
/*     status = RAM_UART_Receive_DMA(&UartHandle, &aDataBuffer[(aNVOffset&7)], 4); */

/*     if ( status != HAL_OK) */
/*     { */
/*       /\* An error occurred *\/ */
/*       break; */
/*     } */

/*     tickstart = RAM_GetTick(); */

/*     while ( bWritten == 0 ) */
/*     { */
/*       tic_time = RAM_GetTick() - tickstart; */
/*       if (tic_time > 10000) */
/*       { */
/*         status = HAL_TIMEOUT; */
/*         break; */
/*       } */
/*     } */

/*     bWritten = 0; */

/*     aNVOffset += 4; */
/*     block_size += 4; */
/*   } */
/*   return status; */
/* } */

/* /\** */
/*   * @brief Receive an amount of data in DMA mode */
/*   * @param huart: uart handle */
/*   * @param p_data: pointer to data buffer */
/*   * @param size: amount of data to be received */
/*   * @note   When the UART parity is enabled (PCE = 1) the data received contain the parity bit. */
/*   * @retval HAL status */
/*   *\/ */
/* RAMFUNC HAL_StatusTypeDef RAM_UART_Receive_DMA(UART_HandleTypeDef *huart, uint8_t *p_data, uint16_t size) */
/* { */
/*   uint32_t *tmp; */

/*   if ((huart->State == HAL_UART_STATE_READY) || (huart->State == HAL_UART_STATE_BUSY_TX)) */
/*   { */
/*     if ((p_data == NULL ) || (size == 0)) */
/*     { */
/*       return HAL_ERROR; */
/*     } */

/*     /\* Process Locked *\/ */
/*     __HAL_LOCK(huart); */

/*     huart->pRxBuffPtr = p_data; */
/*     huart->RxXferSize = size; */

/*     huart->ErrorCode = HAL_UART_ERROR_NONE; */

/*     /\* Check if a transmit process is ongoing or not *\/ */
/*     if (huart->State == HAL_UART_STATE_BUSY_TX) */
/*     { */
/*       huart->State = HAL_UART_STATE_BUSY_TX_RX; */
/*     } */
/*     else */
/*     { */
/*       huart->State = HAL_UART_STATE_BUSY_RX; */
/*     } */

/*     /\* Set the UART DMA transfert complete callback *\/ */
/*     huart->hdmarx->XferCpltCallback = RAM_UART_DMAReceiveCplt; */

/*     /\* Enable the DMA Stream *\/ */
/*     tmp = (uint32_t*) & p_data; */

/* #ifdef STM32L0xx */
/*     RAM_DMA_Start_IT(huart->hdmarx, (uint32_t)&huart->Instance->RDR, *(uint32_t*)tmp, size); */
/* #else */
/*     RAM_DMA_Start_IT(huart->hdmarx, (uint32_t)&huart->Instance->DR, *(uint32_t*)tmp, size); */
/* #endif */

/*     /\* Enable the DMA transfer for the receiver request by setting the DMAR bit */
/*        in the UART CR3 register *\/ */
/*     huart->Instance->CR3 |= USART_CR3_DMAR; */

/*     /\* Process Unlocked *\/ */
/*     __HAL_UNLOCK(huart); */

/*     return HAL_OK; */
/*   } */
/*   else */
/*   { */
/*     return HAL_BUSY; */
/*   } */
/* } */

/* #ifdef STM32L0xx */
/* /\** */
/*   * @brief This function handles UART interrupt request. */
/*   * @param huart: uart handle */
/*   * @retval None */
/*   *\/ */
/* RAMFUNC void RAM_UART_IRQHandler(UART_HandleTypeDef *huart) */
/* { */
/*   /\* UART parity error interrupt occurred ------------------------------------*\/ */
/*   if ((__HAL_UART_GET_IT(huart, UART_IT_PE) != RESET) && (__HAL_UART_GET_IT_SOURCE(huart, UART_IT_PE) != RESET)) */
/*   { */
/*     __HAL_UART_CLEAR_IT(huart, UART_CLEAR_PEF); */

/*     huart->ErrorCode |= HAL_UART_ERROR_PE; */
/*     /\* Set the UART state ready to be able to start again the process *\/ */
/*     huart->State = HAL_UART_STATE_READY; */
/*   } */

/*   /\* UART frame error interrupt occured --------------------------------------*\/ */
/*   if ((__HAL_UART_GET_IT(huart, UART_IT_FE) != RESET) && (__HAL_UART_GET_IT_SOURCE(huart, UART_IT_ERR) != RESET)) */
/*   { */
/*     __HAL_UART_CLEAR_IT(huart, UART_CLEAR_FEF); */

/*     huart->ErrorCode |= HAL_UART_ERROR_FE; */
/*     /\* Set the UART state ready to be able to start again the process *\/ */
/*     huart->State = HAL_UART_STATE_READY; */
/*   } */

/*   /\* UART noise error interrupt occured --------------------------------------*\/ */
/*   if ((__HAL_UART_GET_IT(huart, UART_IT_NE) != RESET) && (__HAL_UART_GET_IT_SOURCE(huart, UART_IT_ERR) != RESET)) */
/*   { */
/*     __HAL_UART_CLEAR_IT(huart, UART_CLEAR_NEF); */

/*     huart->ErrorCode |= HAL_UART_ERROR_NE; */
/*     /\* Set the UART state ready to be able to start again the process *\/ */
/*     huart->State = HAL_UART_STATE_READY; */
/*   } */

/*   /\* UART Over-Run interrupt occured -----------------------------------------*\/ */
/*   if ((__HAL_UART_GET_IT(huart, UART_IT_ORE) != RESET) && (__HAL_UART_GET_IT_SOURCE(huart, UART_IT_ERR) != RESET)) */
/*   { */
/*     __HAL_UART_CLEAR_IT(huart, UART_CLEAR_OREF); */

/*     huart->ErrorCode |= HAL_UART_ERROR_ORE; */
/*     /\* Set the UART state ready to be able to start again the process *\/ */
/*     huart->State = HAL_UART_STATE_READY; */
/*   } */

/*   /\* Call UART Error Call back function if need be --------------------------*\/ */
/*   if (huart->ErrorCode != HAL_UART_ERROR_NONE) */
/*   { */
/*     HAL_UART_ErrorCallback(huart); */
/*   } */
/* } */
/* #endif */

/* #ifdef STM32L1xx */
/* /\** */
/*   * @brief  This function handles UART interrupt request. */
/*   * @param  huart: Pointer to a UART_HandleTypeDef structure that contains */
/*   *                the configuration information for the specified UART module. */
/*   * @retval None */
/*   *\/ */
/* RAMFUNC void RAM_UART_IRQHandler(UART_HandleTypeDef *huart) */
/* { */
/*   uint32_t tmp_flag = 0, tmp_it_source = 0; */

/*   tmp_flag = __HAL_UART_GET_FLAG(huart, UART_FLAG_PE); */
/*   tmp_it_source = __HAL_UART_GET_IT_SOURCE(huart, UART_IT_PE); */
/*   /\* UART parity error interrupt occurred ------------------------------------*\/ */
/*   if ((tmp_flag != RESET) && (tmp_it_source != RESET)) */
/*   { */
/*     huart->ErrorCode |= HAL_UART_ERROR_PE; */
/*   } */

/*   tmp_flag = __HAL_UART_GET_FLAG(huart, UART_FLAG_FE); */
/*   tmp_it_source = __HAL_UART_GET_IT_SOURCE(huart, UART_IT_ERR); */
/*   /\* UART frame error interrupt occurred -------------------------------------*\/ */
/*   if ((tmp_flag != RESET) && (tmp_it_source != RESET)) */
/*   { */
/*     huart->ErrorCode |= HAL_UART_ERROR_FE; */
/*   } */

/*   tmp_flag = __HAL_UART_GET_FLAG(huart, UART_FLAG_NE); */
/*   /\* UART noise error interrupt occurred -------------------------------------*\/ */
/*   if ((tmp_flag != RESET) && (tmp_it_source != RESET)) */
/*   { */
/*     huart->ErrorCode |= HAL_UART_ERROR_NE; */
/*   } */

/*   tmp_flag = __HAL_UART_GET_FLAG(huart, UART_FLAG_ORE); */
/*   /\* UART Over-Run interrupt occurred ----------------------------------------*\/ */
/*   if ((tmp_flag != RESET) && (tmp_it_source != RESET)) */
/*   { */
/*     huart->ErrorCode |= HAL_UART_ERROR_ORE; */
/*   } */

/*   if (huart->ErrorCode != HAL_UART_ERROR_NONE) */
/*   { */
/*     /\* Clear all the error flag at once *\/ */
/*     __HAL_UART_CLEAR_PEFLAG(huart); */

/*     /\* Set the UART state ready to be able to start again the process *\/ */
/*     huart->State = HAL_UART_STATE_READY; */

/*     HAL_UART_ErrorCallback(huart); */
/*   } */
/* } */
/* #endif */

/* /\** */
/*   * @brief  Handles DMA interrupt request. */
/*   * @param  hdma: pointer to a DMA_HandleTypeDef structure that contains */
/*   *               the configuration information for the specified DMA Channel. */
/*   * @retval None */
/*   *\/ */
/* RAMFUNC void RAM_DMA_IRQHandler(DMA_HandleTypeDef *hdma) */
/* { */
/*   /\* Transfer Error Interrupt management ***************************************\/ */
/*   if (__HAL_DMA_GET_FLAG(hdma, __HAL_DMA_GET_TE_FLAG_INDEX(hdma)) != RESET) */
/*   { */
/*     if (__HAL_DMA_GET_IT_SOURCE(hdma, DMA_IT_TE) != RESET) */
/*     { */
/*       /\* Disable the transfer error interrupt *\/ */
/*       __HAL_DMA_DISABLE_IT(hdma, DMA_IT_TE); */

/*       /\* Clear the transfer error flag *\/ */
/*       __HAL_DMA_CLEAR_FLAG(hdma, __HAL_DMA_GET_TE_FLAG_INDEX(hdma)); */

/*       /\* Update error code *\/ */
/*       hdma->ErrorCode |= HAL_DMA_ERROR_TE; */

/*       /\* Change the DMA state *\/ */
/*       hdma->State = HAL_DMA_STATE_ERROR; */

/*       /\* Process Unlocked *\/ */
/*       __HAL_UNLOCK(hdma); */

/*       if (hdma->XferErrorCallback != NULL) */
/*       { */
/*         /\* Transfer error callback *\/ */
/*         hdma->XferErrorCallback(hdma); */
/*       } */
/*     } */
/*   } */

/*   /\* Transfer Complete Interrupt management ***********************************\/ */
/*   if (__HAL_DMA_GET_FLAG(hdma, __HAL_DMA_GET_TC_FLAG_INDEX(hdma)) != RESET) */
/*   { */
/*     if (__HAL_DMA_GET_IT_SOURCE(hdma, DMA_IT_TC) != RESET) */
/*     { */
/*       if ((hdma->Instance->CCR & DMA_CCR_CIRC) == 0) */
/*       { */
/*         /\\* Disable the transfer complete interrupt *\\/ */
/*         __HAL_DMA_DISABLE_IT(hdma, DMA_IT_TC); */
/*       } */
/*       /\\* Clear the transfer complete flag *\\/ */
/*       __HAL_DMA_CLEAR_FLAG(hdma, __HAL_DMA_GET_TC_FLAG_INDEX(hdma)); */

/*       /\\* Update error code *\\/ */
/*       hdma->ErrorCode |= HAL_DMA_ERROR_NONE; */

/*       /\\* Change the DMA state *\\/ */
/*       hdma->State = HAL_DMA_STATE_READY; */

/*       /\\* Process Unlocked *\\/ */
/*       __HAL_UNLOCK(hdma); */

/*       if (hdma->XferCpltCallback != NULL) */
/*       { */
/*         /\\* Transfer complete callback *\\/ */
/*         hdma->XferCpltCallback(hdma); */
/*       } */
/*     } */
/*   } */
/* } */


RAMFUNC void RAM_GPIO_WritePin(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, GPIO_PinState PinState)
{
    /* Check the parameters */
    assert_param(IS_GPIO_PIN_AVAILABLE(GPIOx,GPIO_Pin));
    assert_param(IS_GPIO_PIN_ACTION(PinState));
  
    if (PinState != GPIO_PIN_RESET)
    {
        GPIOx->BSRR = GPIO_Pin;
    }
    else
    {
        GPIOx->BRR = GPIO_Pin ;
    }
}

/**
  * @}
  */

/*********************** (C) COPYRIGHT STMicroelectronics ******END OF FILE****/
