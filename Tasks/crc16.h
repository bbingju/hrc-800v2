#ifndef _CRC16_H_
#define _CRC16_H_

#include <inttypes.h>

#ifdef __cplusplus
extern "C"
{
#endif

unsigned short crc16_ccitt(const uint8_t *buf, int len);

#ifdef __cplusplus
}
#endif

#endif /* _CRC16_H_ */