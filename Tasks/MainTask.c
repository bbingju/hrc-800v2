#include <string.h>

#include "stm32l0xx_hal.h"
#include "cmsis_os.h"
#ifdef FEATURE_RTT_FOR_DEBUG
#include "SEGGER_RTT.h"
#endif

#include "Common.h"
#include "nv.h"

#include <math.h>

//------------------------------------------------------------------------------
#ifdef __cplusplus
extern "C"
{
#endif

    void TaskMain(void const *argument);

#ifdef __cplusplus
}
#endif

extern ADC_HandleTypeDef hadc;
extern IWDG_HandleTypeDef hiwdg;

void UpdateCurrentTemperature();

//------------------------------------------------------------------------------
CURRENT_STATE CurrentState;
// CURRENT_STATE PrevState;

DEVICE_CONFIG DeviceConfig = {
    .RoomNumber = 1,
    /* .CurrentRoomTemperatureOffset = { */
    /*     -1.0f, -1.0f, -1.0f, -1.0f, */
    /* } */
};

uint32_t AD_Count = 0;

//------------------------------------------------------------------------------
void TaskMain(void const *argument)
{
    DBG_LOG("-------------- HRC-800V2 --------------------\r\n");

    for (int i = 0; i < 8; i++)
    {
        ROOM *r = &CurrentState.rooms[i];
        r->id = i + 1;
        r->heat_mode = HEAT_MODE_OFF;
        r->coil_length = COIL_LENGTH_DEFAULT;
        r->CurrentTemperature = 0.0f;
        r->SettingTemperature = SETTING_TEMP_DEFAULT;
        r->reservation_mode_on = false;
        r->reserved_time_block = 0;
    }

    CLEAR_MODIFIED_BITS(CurrentState.modified_bits);
    CLEAR_MODIFIED_BITS(CurrentState.modified_bits_heat_mode);

    nv_init(&CurrentState, &DeviceConfig);

    CurrentState.room = &CurrentState.rooms[DeviceConfig.RoomNumber - 1];
    CurrentState.coil_length_room = NULL;

    if (CurrentState.room->id == 1) {
        nv_load_coil_length(&CurrentState);
        CurrentState.coil_length_room = &CurrentState.rooms[0];
    }

    HAL_ADC_Init(&hadc);
    HAL_ADCEx_Calibration_Start(&hadc, ADC_SINGLE_ENDED);

    vTaskDelay(100);

    // CurrentState.rooms[DeviceConfig.RoomNumber - 1].CurrentTemperature = -99.0f;

    while(true)
    {
      UpdateCurrentTemperature();

      if (AD_Count > 1000)
        break;
    }

    for (;;)
    {
        HAL_IWDG_Refresh(&hiwdg);

        UpdateCurrentTemperature();
        osDelay(50);
    }
}

// Exponential Moving Average(EMA) 필터 적용
#define DSP_EMA_I32_ALPHA(x) ((uint16_t)(x * 65535))
int32_t dsp_ema_i32(int32_t in, int32_t average, uint16_t alpha)
{
    int64_t tmp0; //calcs must be done in 64-bit math to avoid overflow
    tmp0 = (int64_t)in * (alpha) + (int64_t)average * (65536 - alpha);
    return (int32_t)((tmp0 + 32768) / 65536); //scale back to 32-bit (with rounding)
}

const float Ra = 0.000861400768070162;
const float Rb = 0.000256375854719805;
const float Rc = 0.000000168058358084032;

void UpdateCurrentTemperature()
{
    static int32_t average = 0;

    HAL_ADC_Start(&hadc);
    if (HAL_ADC_PollForConversion(&hadc, 10) == HAL_OK)
    {
        AD_Count++;
        uint32_t adc_val = HAL_ADC_GetValue(&hadc);

        if (average == 0)
        {
            average = dsp_ema_i32(adc_val, average, DSP_EMA_I32_ALPHA(0.01));
        }
        else
        {
            // EMA 필터 적용한 평균값, Alpha = 0.01
            average = dsp_ema_i32(adc_val, average, DSP_EMA_I32_ALPHA(0.01));
        }

        /* float raw_voltage = (3.3f / 4096) * average; */
        float Rt, /* Adc, */ T;

        Rt = ((4096 * 10000 / average) - 10000); // NTC 서미스터 저항값

        float log_r = log(Rt);
        float log_r3 = log_r * log_r * log_r;

        // Stein-Hart식 적용
        T = 1.0f / (Ra + (Rb * log_r) + (Rc * log_r3)) - 273.15;

        //printf("%u %3.3f\r\n", adc_val, T);

        if (AD_Count > 1000)
        {
            CurrentState.rooms[DeviceConfig.RoomNumber - 1].CurrentTemperature = T + CurrentState.temp_correction;
        }
    }
    HAL_ADC_Stop(&hadc);
}
