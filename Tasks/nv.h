#pragma once

#include "Common.h"

#ifdef __cplusplus
extern "C" {
#endif

    extern void nv_init(CURRENT_STATE *state, DEVICE_CONFIG *devconf);
    extern int nv_load(CURRENT_STATE *state, DEVICE_CONFIG *device);
    extern int nv_save(CURRENT_STATE *state, DEVICE_CONFIG *device);
    extern int nv_load_room_related(CURRENT_STATE *state, DEVICE_CONFIG *devconf);

    extern int nv_load_coil_length(CURRENT_STATE *state);
    extern int nv_save_coil_length(CURRENT_STATE *state);

#ifdef __cplusplus
}
#endif

