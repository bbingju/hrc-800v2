/**
  ******************************************************************************
  * @file    EEPROM-ComWhileWrite/inc/ramfunc.h
  * @author  MCD Application Team
  * @version V0.1.0
  * @date    16th February 2016
  * @brief   This file provides all the headers of the flash_if functions.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2016 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __RAMFUNC_H
#define __RAMFUNC_H

/* Includes ------------------------------------------------------------------*/
#include "stm32l0xx_hal.h"
#include "main.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
#define USER_EEPROM_SIZE ((uint32_t)2*1024)
#define DATA_ADDRESS ((uint32_t)0x08080000)

/* #ifdef STM32L073xx */
/* #define USER_EEPROM_SIZE ((uint32_t)6*1024) */
/* #define DATA_ADDRESS ((uint32_t)0x08080000) */
/* #else */
/* #ifdef STM32L152xB */
/* #define USER_EEPROM_SIZE ((uint32_t)4*1024) */
/* #define DATA_ADDRESS ((uint32_t)0x08080000) */
/* #else */
/* #ifdef STM32L152xD */
/* #define USER_EEPROM_SIZE ((uint32_t)12*1024) */
/* #define DATA_ADDRESS ((uint32_t)0x08080000) */
/* #else */
/* #error "Please select first the STM32 device to be used (in stm32l0xx.h)" */
/* #endif */
/* #endif */
/* #endif */

/* Exported varaibles --------------------------------------------------------*/
extern uint32_t aNVOffset;
/* #ifdef STM32L073xx */
/* extern FLASH_ProcessTypeDef ProcFlash; */
/* #else */
extern FLASH_ProcessTypeDef pFlash;
/* #endif */

#define __HAL_UART_CLEAR_IT(__HANDLE__, __IT_CLEAR__) ((__HANDLE__)->Instance->ICR = (uint32_t)(__IT_CLEAR__))

/* Exported functions ------------------------------------------------------- */
/* RAMFUNC void RAM_UART_RxCpltCallback(UART_HandleTypeDef *huart); */
/* RAMFUNC HAL_StatusTypeDef RAM_UART_Receive_DMA(UART_HandleTypeDef *huart, uint8_t *p_data, uint16_t size); */
/* RAMFUNC void RAM_UART_IRQHandler(UART_HandleTypeDef *huart); */
/* RAMFUNC void RAM_DMA_IRQHandler(DMA_HandleTypeDef *hdma); */
/* RAMFUNC HAL_StatusTypeDef RAMReceivePacket( uint32_t packet_size); */
RAMFUNC HAL_StatusTypeDef RAM_FLASHEx_DATAEEPROM_Program(uint32_t address, uint32_t data);
RAMFUNC void RAM_GPIO_WritePin(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, GPIO_PinState PinState);

#endif  /* __RAMFUNC_H */

/*******************(C)COPYRIGHT 2016 STMicroelectronics *****END OF FILE******/
