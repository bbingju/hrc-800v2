#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "stm32l0xx_hal.h"
#include "nv.h"
#include "ramfunc.h"

#define FEATURE_RAM_FUNCTION
//#define FEATURE_SAVE_CURRENT_STATE

#define EEPROM_ADDRESS_ROOM_NUMBER    (DATA_EEPROM_BASE + (128 * 0))
#define EEPROM_ADDRESS_STATE          (DATA_EEPROM_BASE + (128 * 1))
#define EEPROM_ADDRESS_COIL_LENGTH    (DATA_EEPROM_BASE + (128 * 2))
#define EEPROM_ADDRESS_INFO_TO_SAVE   (DATA_EEPROM_BASE + (128 * 3))
#define EEPROM_ADDRESS_RESET          (DATA_EEPROM_BASE + (128 * 15))
#define NV_RESET_MAGIC                0xdeadbeef

#ifdef FEATURE_RAM_FUNCTION
#define FLASH_PROGRAM(a, d)           RAM_FLASHEx_DATAEEPROM_Program(a, d)
#else
#define FLASH_PROGRAM(a, d)           HAL_DATA_EEPROMEx_Program(FLASH_TYPEPROGRAMDATA_WORD, a, d)
#endif

static int nv_is_init()
{
    uint32_t blob = 0;
    memcpy(&blob, (void*)(EEPROM_ADDRESS_RESET), 4);
    return (blob == NV_RESET_MAGIC);
}

static void nv_set_defaults()
{
    /* __disable_irq(); */
    HAL_DATA_EEPROMEx_Unlock();
    HAL_DATA_EEPROMEx_Erase(EEPROM_ADDRESS_ROOM_NUMBER);
    FLASH_PROGRAM(EEPROM_ADDRESS_ROOM_NUMBER + 0, 1);
    FLASH_PROGRAM(EEPROM_ADDRESS_ROOM_NUMBER + 4, ROOM_NBR_MAX); /* total_rooms */
    /* HAL_DATA_EEPROMEx_Lock(); */
    /* __enable_irq(); */

    /* __disable_irq(); */
    /* HAL_DATA_EEPROMEx_Unlock(); */
    HAL_DATA_EEPROMEx_Erase(EEPROM_ADDRESS_STATE);
    FLASH_PROGRAM(EEPROM_ADDRESS_STATE + 0, (uint32_t)((float)SETTING_TEMP_DEFAULT * 10.0f));
    FLASH_PROGRAM(EEPROM_ADDRESS_STATE + 4, 0); /* heat_mode_on */
    FLASH_PROGRAM(EEPROM_ADDRESS_STATE + 8, 0); /* reservation_mode_on */
    FLASH_PROGRAM(EEPROM_ADDRESS_STATE + 12, 0); /* leave_mode_on */
    FLASH_PROGRAM(EEPROM_ADDRESS_STATE + 16, 0); /* reserved_time_block */
    FLASH_PROGRAM(EEPROM_ADDRESS_STATE + 20, 0); /* temp_correction */
    /* HAL_DATA_EEPROMEx_Lock(); */
    /* __enable_irq(); */

    /* __disable_irq(); */
    /* HAL_DATA_EEPROMEx_Unlock(); */
    HAL_DATA_EEPROMEx_Erase(EEPROM_ADDRESS_COIL_LENGTH);
    for (int i = 0; i < 8; i++) {
        FLASH_PROGRAM(EEPROM_ADDRESS_COIL_LENGTH + (i * 4), COIL_LENGTH_DEFAULT);
    }
    /* HAL_DATA_EEPROMEx_Lock(); */
    /* __enable_irq(); */

    /* HAL_DATA_EEPROMEx_Unlock(); */
    HAL_DATA_EEPROMEx_Erase(EEPROM_ADDRESS_INFO_TO_SAVE);
    FLASH_PROGRAM(EEPROM_ADDRESS_INFO_TO_SAVE + 0, 0); /* reserved_time_block */
    FLASH_PROGRAM(EEPROM_ADDRESS_INFO_TO_SAVE + 4, 0); /* temp_correction */
    /* HAL_DATA_EEPROMEx_Lock(); */

    /* set reset value */
    /* __disable_irq(); */
    /* HAL_DATA_EEPROMEx_Unlock(); */
    HAL_DATA_EEPROMEx_Erase(EEPROM_ADDRESS_RESET);
    FLASH_PROGRAM(EEPROM_ADDRESS_RESET, NV_RESET_MAGIC);
    HAL_DATA_EEPROMEx_Lock();
    /* __enable_irq(); */
}

void nv_init(CURRENT_STATE *state, DEVICE_CONFIG *devconf)
{
    if (!nv_is_init()) {
        nv_set_defaults();
    }

    nv_load(state, devconf);
}


int nv_load_room_related(CURRENT_STATE *state, DEVICE_CONFIG *devconf)
{
    uint32_t blob;

    if (!state || !devconf)
        return -1;

    /* Total number of rooms */
    memcpy(&blob, (void*)(EEPROM_ADDRESS_ROOM_NUMBER + 4), 4);
    /* printf("%s: read total rooms %u\r\n", __func__, blob); */
    devconf->total_rooms = (blob <= 0 || blob > 8) ? 8 : blob;

    /* Room number */
    memcpy(&blob, (void*)(EEPROM_ADDRESS_ROOM_NUMBER + 0), 4);
    /* printf("%s: read room number %u\r\n", __func__, blob); */
    devconf->RoomNumber = (blob <= 0 || blob > 8) ? 1 : blob;

    return 0;
}

int nv_load(CURRENT_STATE *state, DEVICE_CONFIG *devconf)
{
    uint32_t blob;

    if (!state || !devconf)
        return -1;

    nv_load_room_related(state, devconf);

#ifdef FEATURE_SAVE_CURRENT_STATE
    /* Setting Temperature */
    memcpy(&blob, (void*)(EEPROM_ADDRESS_STATE + 0), 4);
    state->rooms[devconf->RoomNumber - 1].SettingTemperature = ((float)blob) / 10.0f;

    memcpy(&blob, (void*)(EEPROM_ADDRESS_STATE + 4), 4);
    state->rooms[devconf->RoomNumber - 1].heat_mode = blob;

    /* Room reservation_mode_on */
    memcpy(&blob, (void*)(EEPROM_ADDRESS_STATE + 8), 4);
    /* printf("%s: read reservation_mode_on %u\r\n", __func__, blob); */
    state->rooms[devconf->RoomNumber - 1].reservation_mode_on = blob ? true : false;

    /* Room leave_mode_on */
    memcpy(&blob, (void*)(EEPROM_ADDRESS_STATE + 12), 4);
    /* printf("%s: read leave_mode_on %u\r\n", __func__, blob); */
    state->leave_mode_on = blob ? true : false;
#else
    state->rooms[devconf->RoomNumber - 1].SettingTemperature = 18.0f;
    state->rooms[devconf->RoomNumber - 1].heat_mode = HEAT_MODE_OFF;
    state->rooms[devconf->RoomNumber - 1].reservation_mode_on = false;
    state->leave_mode_on = false;
#endif  /* FEATURE_SAVE_CURRENT_STATE */

    /* Room reserved_time_block */
    memcpy(&blob, (void*)(EEPROM_ADDRESS_INFO_TO_SAVE + 0), 4);
    state->rooms[devconf->RoomNumber - 1].reserved_time_block = blob;

    /* Temperature Correction ( -5.0 ~ 5.0 ) */
    memcpy(&blob, (void*)(EEPROM_ADDRESS_INFO_TO_SAVE + 4), 4);
    state->temp_correction = ((float)blob) / 10.0f;

    return 0;
}

int nv_save(CURRENT_STATE *state, DEVICE_CONFIG *devconf)
{
    if (!state || !devconf)
        return -1;

    DEVICE_CONFIG devconf_from_nv;
    CURRENT_STATE state_from_nv;
    ROOM *self_room = &state->rooms[devconf->RoomNumber - 1];

    memcpy(&devconf_from_nv, devconf, sizeof(DEVICE_CONFIG));
    memcpy(&state_from_nv, state, sizeof(CURRENT_STATE));
    nv_load(&state_from_nv, &devconf_from_nv);

    if (memcmp(&devconf_from_nv, devconf, sizeof(DEVICE_CONFIG)) != 0)
    {
        /* __disable_irq(); */
        HAL_DATA_EEPROMEx_Unlock();
        HAL_DATA_EEPROMEx_Erase(EEPROM_ADDRESS_ROOM_NUMBER);
        FLASH_PROGRAM(EEPROM_ADDRESS_ROOM_NUMBER + 0, devconf->RoomNumber);
        FLASH_PROGRAM(EEPROM_ADDRESS_ROOM_NUMBER + 4, devconf->total_rooms);
        HAL_DATA_EEPROMEx_Lock();
        /* __enable_irq(); */
        /* DBG_LOG("%s: device config is saved\r\n", __func__); */
    }

#ifdef FEATURE_SAVE_CURRENT_STATE
    if (memcmp(&state_from_nv, state, sizeof(CURRENT_STATE)) != 0)
    {
        /* __disable_irq(); */
        HAL_DATA_EEPROMEx_Unlock();
        HAL_DATA_EEPROMEx_Erase(EEPROM_ADDRESS_STATE);
        FLASH_PROGRAM(EEPROM_ADDRESS_STATE + 0, (uint32_t)((float)self_room->SettingTemperature * 10.0f));
        FLASH_PROGRAM(EEPROM_ADDRESS_STATE + 4, self_room->heat_mode);
        FLASH_PROGRAM(EEPROM_ADDRESS_STATE + 8, self_room->reservation_mode_on);
        FLASH_PROGRAM(EEPROM_ADDRESS_STATE + 12, state->leave_mode_on);
        HAL_DATA_EEPROMEx_Lock();
        /* __enable_irq(); */
        /* DBG_LOG("%s: current state is saved\r\n", __func__); */
    }
#endif  /* FEATURE_SAVE_CURRENT_STATE */


    if ((self_room->reserved_time_block != state_from_nv.rooms[devconf->RoomNumber - 1].reserved_time_block) ||
        (state->temp_correction != state_from_nv.temp_correction))
    {
        HAL_DATA_EEPROMEx_Unlock();
        HAL_DATA_EEPROMEx_Erase(EEPROM_ADDRESS_INFO_TO_SAVE);
        FLASH_PROGRAM(EEPROM_ADDRESS_INFO_TO_SAVE + 0, self_room->reserved_time_block);
        FLASH_PROGRAM(EEPROM_ADDRESS_INFO_TO_SAVE + 4, (uint32_t)((float)state->temp_correction * 10.0f));
        HAL_DATA_EEPROMEx_Lock();
    }
    return 0;
}

int nv_load_coil_length(CURRENT_STATE *state)
{
    uint32_t blob;

    if (!state)
        return -1;

    for (int i = 0; i < 8; i++) {
        memcpy(&blob, (void*)(EEPROM_ADDRESS_COIL_LENGTH + (i * 4)), 4);
        if (blob >= 0 && blob <= 40)
            state->rooms[i].coil_length = blob;
        else
            state->rooms[i].coil_length = COIL_LENGTH_DEFAULT;
    }

    return 0;
}

int nv_save_coil_length(CURRENT_STATE *state)
{
    bool different = false;
    uint32_t blob;

    if (!state)
        return -1;

    uint16_t coil_length_from_nv[8] = { 0 };

    for (int i = 0; i < 8; i++) {
        memcpy(&blob, (void*)(EEPROM_ADDRESS_COIL_LENGTH + (i * 4)), 4);
        coil_length_from_nv[i] = blob;
    }

    for (int i = 0; i < 8; i++) {
        if (coil_length_from_nv[i] != state->rooms[i].coil_length) {
            different = true;
            break;
        }
    }

    if (different) {
        /* __disable_irq(); */
        HAL_DATA_EEPROMEx_Unlock();
        HAL_DATA_EEPROMEx_Erase(EEPROM_ADDRESS_COIL_LENGTH);
        for (int i = 0; i < 8; i++) {
            FLASH_PROGRAM(EEPROM_ADDRESS_COIL_LENGTH + (i * 4),
                          state->rooms[i].coil_length);
        }
        HAL_DATA_EEPROMEx_Lock();
        /* __enable_irq(); */
    }

    return 0;
}
