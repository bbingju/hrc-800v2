#pragma once

#include <stdint.h>
#include <stdbool.h>
#include "stm32l0xx_hal.h"
#include "7Segment.h"

typedef struct
{
    int8_t com;
    int8_t seg;
} LCD_MAP;

const static LCD_MAP LCD_SettingTemp_MAP[4][7] = {
	// Digit 1
	{
		//    A               B               C               D               E               F               G
		{1, 18},       {2, 18},        {3,18},         {4, 18},         {4, 17},        {2, 17},        {3, 17}
	},
	// Digit 2
	{
		//    A               B               C               D               E               F               G
		{1, 20},        {2, 20},        {3, 20},        {4, 20},        {4, 19},        {2, 19},        {3, 19}
	},
	// Digit 3
	{
		//    A               B               C               D               E               F               G
		{1, 23},        {2, 23},        {3, 23},        {4, 23},        {4, 22},        {2, 22},        {3, 22}
	},
	// Digit 4
	{
		//    A               B               C               D               E               F               G
		{1, 25},        {2, 25},        {3, 25},        {4, 25},        {4, 24},        {2, 24},        {3, 24}
	},
};

const static LCD_MAP LCD_CurrentTemp_MAP[4][7] = {
	// Digit 1
	{
		//    A               B               C               D               E               F               G
		{-1, -1},       {1, 10},        {1,10},         {-1,-1},       {-1,-1},         {-1,-1},        {-1,-1}
	},
	// Digit 2
	{
		//    A               B               C               D               E               F               G
		{1, 9},         {2, 9},         {3, 9},         {4, 9},         {4, 10},        {2, 10},        {3, 10}
	},
	// Digit 3
	{
		//    A               B               C               D               E               F               G
		{1, 7},         {2, 7},         {3, 7},         {4, 7},         {3, 8},         {1, 8},         {2, 8}
	},
	// Digit 4
	{
		//    A               B               C               D               E               F               G
		{1, 5},         {2, 5},        {3, 5},          {4, 5},         {3, 6},         {1, 6},         {2, 6}
	},
};

const static LCD_MAP LCD_RoomNumber_MAP[1][7] = {
  // Digit 1
  {
  //    A               B               C               D               E               F               G
	{1, 1},        {2, 1},         {3, 1},         {4, 1},          {3, 2},         {1, 2},        {2, 2}
  },
};


const static LCD_MAP LCD_CurrentTempDotPoint_MAP = { 4, 8};

const static LCD_MAP LCD_SettingTempDotPoint_MAP     = {4, 21};
const static LCD_MAP LCD_SettingTempDotPoint2_MAP[2] = { {2, 21}, {3, 21} };

const static LCD_MAP LCD_X_Segments_MAP[] =
{
  // 1      2       3       4       5       6       7       8         9        10
    {4,4},  {3,4},  {2,4},  {1,4},  {1,3},  {2,3},  {3,3},  {-1,-1},  {4,3},   {4,2},
  // 11     12      13      14      15      16      17      18        19       20
  {-1,-1},  {1,17}, {1,19}, {3,26}, {4,26}, {2,26}, {1,24}, {-1,-1},  {4,8},   {1,21},
  // 21     22      23      24      25      26
  {-1,-1},  {2,21}, {3,21}, {4,21}, {1,22}, {1,24},
};

const static LCD_MAP LCD_B_Segments_MAP[] =
{
  // 1      2       3       4       5       6       7       8       9       10
    {4,11}, {3,11}, {2,11}, {1,11}, {1,12}, {2,12}, {3,12}, {4,12}, {4,13}, {3,13},
  // 11     12      13      14      15      16      17      18      19      20
    {2,13}, {1,13}, {4,14}, {3,14}, {2,14}, {1,14}, {1,15}, {2,15}, {3,15}, {4,15},
  // 21     22      23      24
    {4,16}, {3,16}, {2,16}, {1,16},
};

//------------------------------------------------------------------------------
extern LCD_HandleTypeDef hlcd;

__INLINE static int GetLCDRegisterNumber(int com)
{
	const int com_mapping[] = {3, 2, 1, 0};
	int result = (2 * com_mapping[com - 1]);

	return result;
}

__INLINE static int GetLCDRegisterBitNumber(int seg)
{
	const int seg_mapping[] =
		{ 0, 1, 3, 4, 5, 6, 7, 8, 9, 10,
		  11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
		  21, 22, 23, 24, 25, 26 };

	int result = seg_mapping[seg - 1];

	return result;
}

__INLINE static void LCD_SetSegmentEx(int8_t com, int8_t seg, bool clear)
{
	if (com < 0 || seg < 0)
		return;
	uint32_t data = (1 << GetLCDRegisterBitNumber(seg));
	uint32_t mask = clear ? ~data : 0xFFFFFFFF;
	data = clear ? 0 : data;
	HAL_LCD_Write(&hlcd, GetLCDRegisterNumber(com), mask, data);
}

__INLINE static void LCD_WriteChar(const LCD_MAP map[7], char value)
{
	int char_map = SegmentLED_CharMap[value];
	int8_t com, seg;
	for (int i=0; i<7; i++) {
		com = map[i].com;
		seg = map[i].seg;
		LCD_SetSegmentEx(com, seg, (char_map & (1<<(6-i))) ? false : true);
	}
}

__INLINE static void draw_4digit(const LCD_MAP map[][7], const char *_4digit)
{
    for (int i = 0; i < 4; i++)
        LCD_WriteChar(map[i], *(_4digit + i));
}

__INLINE static void LCD_Update_X_Segment(int X_seg_index, bool value)
{
	if (X_seg_index > 26 || X_seg_index < 1)
	{
		// Error
		while(true);
	}

	const LCD_MAP *map = &LCD_X_Segments_MAP[X_seg_index - 1];
	LCD_SetSegmentEx(map->com, map->seg, !value);
}


__INLINE static void LCD_Update_B_Segments(uint32_t block, int selecting_index, bool enabled)
{
	if (selecting_index < -1 || selecting_index >= 24)
	{
		// Error
		/* printf("%s: param error (%u)\r\n", __func__, selecting_index); */
		selecting_index = 0;
		/* while(true); */
	}

	for (int i=0; i<24; i++) {
		const LCD_MAP *map = &LCD_B_Segments_MAP[i];
		LCD_SetSegmentEx(map->com, map->seg, true);
	}

	if (enabled) {
		for (int i=0; i<24; i++) {
			if ((block & (1 << i)) && selecting_index != i) {
				const LCD_MAP *map = &LCD_B_Segments_MAP[i];
				LCD_SetSegmentEx(map->com, map->seg, false);
			}
		}
	}
}

__INLINE static void LCD_Update_B_Segment_By_Index(int index, bool enabled)
{
    if (index < 0 || index > 23)
    {
	// Error
	while(true);
    }

    LCD_MAP const *map;

    if (enabled)
    {
	map = &LCD_B_Segments_MAP[index];
	LCD_SetSegmentEx(map->com, map->seg, false);
    }
}

//------------------------------------------------------------------------------
#define LCD__CURRENT_TEMP(enable)            LCD_Update_X_Segment(10, enable)
#define LCD__CURRENT_TEMP_POINT(enable)      LCD_Update_X_Segment(19, enable)
#define LCD__SETTING_TEMP(enable)            LCD_Update_X_Segment(20, enable)
#define LCD__SETTING_TEMP_POINT(enable)      LCD_Update_X_Segment(24, enable)
#define LCD__TEMP_C(enable)                  LCD_Update_X_Segment(25, enable)
#define LCD__TIME_SEPERATOR(enable) do {        \
        LCD_Update_X_Segment(22, enable);       \
        LCD_Update_X_Segment(23, enable);       \
    } while (0);

#define draw_4digit_left(digits)         draw_4digit(LCD_CurrentTemp_MAP, digits)
#define draw_4digit_right(digits)        draw_4digit(LCD_SettingTemp_MAP, digits)
