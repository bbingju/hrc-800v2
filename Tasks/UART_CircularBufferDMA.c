#include "UART_CircularBufferDMA.h"

inline static uint32_t GetDMA_WritePTR(struct _UART_CircularBufferDMA *obj)
{
	/* NDTR = Number of Data */
	return ((obj->BufferSize - obj->handle->hdmarx->Instance->CNDTR) & (obj->BufferSize - 1));
}

static bool _is_empty(struct _UART_CircularBufferDMA *obj)
{
	if (!obj)
		return false;

	return obj->ReadPtr == GetDMA_WritePTR(obj);
}

static int _receive(struct _UART_CircularBufferDMA *obj)
{
	if (!obj)
		return -1;

	uint8_t c = 0;
	if (obj->ReadPtr != GetDMA_WritePTR(obj))
	{
		c = obj->Buffer[obj->ReadPtr++];
		obj->ReadPtr &= (obj->BufferSize - 1);
		return c;
	}
	else
	{
		return -1;
	}
}

int UART_CircularBufferDMA_init(struct _UART_CircularBufferDMA *obj,
				UART_HandleTypeDef *handle,
				uint8_t* _Buffer,
				uint32_t _BufferSize)
{
	HAL_StatusTypeDef ret;

	if (!obj)
		return -1;

	obj->handle     = handle;
	obj->ReadPtr    = 0;
	obj->Buffer     = _Buffer;
	obj->BufferSize = _BufferSize;

	obj->is_empty   = _is_empty;
	obj->receive    = _receive;

	ret = HAL_UART_Receive_DMA(obj->handle,
				obj->Buffer,
				obj->BufferSize);
	return (ret == HAL_OK) ? 0 : -1;
}

